# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 13:41:49 2015
@author: tchierici
"""

import os
import re
import HTMLParser
import sys
import unicodedata
import pandas as pd
from io import StringIO
from azure.storage.blob import BlobService

reload(sys)
sys.setdefaultencoding('utf8')

# Classe que representa cada artigo mapeado
class Artigo(object):
    idSite = "NE"
    doi = "NE"
    titulo = "NE"
    autores = []
    dataAceite = 1 / 1 / 1900
    dataPublicacao = 1 / 1 / 1900
    dataSubmissao = 1 / 1 / 1900

    # The class "constructor" - It's actually an initializer 
    def __init__(self, doi, titulo="", autores=[], dataPublicacao=1 / 1 / 1900, dataSubmissao=1 / 1 / 1900,
                 dataAceite=1 / 1 / 1900):
        global tamanhoCodSite
        self.idSite = doi[:tamanhoCodSite]
        self.doi = doi
        self.titulo = titulo
        self.autores = autores
        self.dataPublicacao = dataPublicacao
        self.dataSubmissao = dataSubmissao
        self.dataAceite = dataAceite


# Classe que carregará as expressões regulares que serão utilizadas no processamento dos artigos
class AnalisadorSite(object):
    idSite = ""
    expressaoTitulo = ""
    expressaoAutores = ""
    expressaoDataAceite = ""
    expressaoDataPublicacao = ""
    expressaoDataSubmissao = ""

    # The class "constructor" - It's actually an initializer
    def __init__(self, idSite, titulo="", autores="", dataPublicacao="", dataSubmissao="", dataAceite=""):
        self.idSite = idSite
        self.expressaoTitulo = titulo
        self.expressaoAutores = autores
        if type(dataPublicacao) == type(str()) or type(dataPublicacao) == type(unicode()):
            self.expressaoDataPublicacao = dataPublicacao
        if type(dataSubmissao) == type(str()) or type(dataSubmissao) == type(unicode()):
            self.expressaoDataSubmissao = dataSubmissao
        if type(dataAceite) == type(str()) or type(dataAceite) == type(unicode()):
            self.expressaoDataAceite = dataAceite

    def analisar_arquivo(self, doi, caminhoArquivo):
        global execucaoNaNuvem
        global blob_service
        if execucaoNaNuvem:
            conteudo = blob_service.get_blob('doi', caminhoArquivo)
        else:
            arquivo = open(caminhoArquivo)
            conteudo = arquivo.read()
            arquivo.close()
        
        return self.analisar_artigo(doi, conteudo)

    def analisar_artigo(self, doi, conteudoHTML):
        """Analisa o conteudo fornecido de acordo com as expressões regulares 
        do objeto instanciado. Retorna um objeto Artigo com as propriedades
        encontradas
    
        Keyword arguments:
        doi -- Identificador universal do artigo
        conteudoHTML -- texto a ser analizado em busca dos metadados do artigo
        """

        # Inicializa o artigo de retorno
        artigoRetorno = Artigo(doi)

        print 'doi', doi

        if doi[:6] == "101016":
            # Expressão não funcionava para alguns htmls que vinham com quebras diferentes dos demais
            print 'Substituicao de quebras de linha'
            conteudoHTML = str.replace(conteudoHTML, '\n', ' ')
            conteudoHTML = str.replace(conteudoHTML, '\t', ' ')
            conteudoHTML = str.replace(conteudoHTML, '\r', ' ')

        if doi == "101093bioinformaticsbtq592":
            doi = "101093bioinformaticsbtq592"  # investigar 101016jajodo200802018

        # Busca o título do artigo
        if self.expressaoTitulo:
            m = re.findall(self.expressaoTitulo, conteudoHTML)
            if m:
                if type(m[0]) == type(tuple()):
                    artigoRetorno.titulo = m[0][1]
                else:
                    artigoRetorno.titulo = m[0]
            else:
                print 'Titulo nao encontrado - Doi:', doi, ' Expressao:', self.expressaoTitulo

        # Busca autores do artigo
        if self.expressaoAutores:
            m = re.findall(self.expressaoAutores, conteudoHTML)
            if m:
                artigoRetorno.autores = m
            else:
                print 'Autores nao encontrados - Doi:', doi, ' Expressao:', self.expressaoAutores

        # Busca a data de publicação do artigo
        if self.expressaoDataPublicacao:

            # Se é uma lista de expressões:
            if self.expressaoDataPublicacao[0:1] == "|":
                expressoes = unicode.split(self.expressaoDataPublicacao, "|")
                # pra cada expressão
                for exp in expressoes:
                    # se não está vazia
                    if exp:
                        print exp, 'doi', doi
                        m = re.findall(exp, conteudoHTML)
                        if m:
                            # quando temos expressões com alternativas, pegamos a que encontrou alguma coisa
                            if type(m[0]) == type(tuple()):
                                if m[0][len(m[0]) - 1]:
                                    artigoRetorno.dataPublicacao = m[0][len(m[0]) - 1]
                                else:
                                    artigoRetorno.dataPublicacao = m[0][1]
                            else:
                                artigoRetorno.dataPublicacao = m[0]
                            break

                            #print 'Publicacao nao encontrada - Doi:', doi, ' Expressao:', self.expressaoDataPublicacao

            else:
                print self.expressaoDataPublicacao, 'doi', doi
                m = re.findall(self.expressaoDataPublicacao, conteudoHTML)
                if m:
                    # quando temos expressões com alternativas, pegamos a que encontrou alguma coisa
                    if type(m[0]) == type(tuple()):
                        if m[0][len(m[0]) - 1]:
                            artigoRetorno.dataPublicacao = m[0][len(m[0]) - 1]
                        else:
                            artigoRetorno.dataPublicacao = m[0][1]
                    else:
                        artigoRetorno.dataPublicacao = m[0]
                else:
                    print 'Publicacao nao encontrada - Doi:', doi, ' Expressao:', self.expressaoDataPublicacao

        # Busca a data de aceite
        if self.expressaoDataAceite:
            # tenta com expressão alternativa (aceite)
            m = re.findall(self.expressaoDataAceite, conteudoHTML)
            if m:
                # quando temos expressões com alternativas, pegamos a que encontrou alguma coisa
                if type(m[0]) == type(tuple()):
                    if m[0][1]:
                        artigoRetorno.dataAceite = m[0][1]
                    else:
                        artigoRetorno.dataAceite = m[0][len(m[0]) - 1]
                else:
                    artigoRetorno.dataAceite = m[0]
            else:
                print 'Aceite nao encontrada - Doi:', doi, ' Expressao:', self.expressaoDataAceite

        # Busca a data de submissão do artigo
        if self.expressaoDataSubmissao:
            m = re.findall(self.expressaoDataSubmissao, conteudoHTML)
            if m:
                artigoRetorno.dataSubmissao = m[0]
            else:
                print 'Submissao nao encontrada - Doi:', doi, ' Expressao:', self.expressaoDataSubmissao

        return artigoRetorno


# Fabric de analisadores de site
class AnalisadorFabric(object):
    _listaExpressoes = {}

    # Construtor que busca as expressões do arquivo csv
    def __init__(self):
        # Referencia a variável global que injeta o caminho do arquivo com regex
        global caminhoArquivoExpressoes
        global blob_service
        global execucaoNaNuvem

        if execucaoNaNuvem:
            conteudoExpressoes = StringIO(blob_service.get_blob_to_text('consolidado', 'expressoes.csv', text_encoding='latin_1'))
        else:
            conteudoExpressoes = caminhoArquivoExpressoes

        # TODO: Header=0 ou header=None
        arquivoExpressoes = pd.read_csv(conteudoExpressoes, sep=';', index_col=0, header=0, encoding='latin_1',
                                        na_values=' ')
        # Retira as linhas vazias (sem Doi)        
        expressoesFiltradas = arquivoExpressoes[pd.notnull(arquivoExpressoes.CodigoSite)]

        # print arquivoExpressoes.as_matrix()
        for index, row in expressoesFiltradas.iterrows():
            item = AnalisadorSite(row.CodigoSite, row.Titulo, row.Autor, row.DataPublicacao, row.DataSubmissao,
                                  row.DataAceite)
            self._listaExpressoes[item.idSite] = item

    # Retorna uma instância de analisador de site
    def criar_analisador(self, doi):
        global tamanhoCodSite
        chavedoi = doi[:tamanhoCodSite]

        # Retorna o objeto de expressões baseado nos 6 primeiros digitos do doi
        if chavedoi in self._listaExpressoes.keys():
            return self._listaExpressoes[chavedoi]
        else:
            print 'REGEX faltante: ', chavedoi
            return self._listaExpressoes[self._listaExpressoes.keys()[0]]


def escreve_dado(texto):
    inicioDado = "\""
    fimDado = "\""
    texto = texto.replace("\"", "'")
    html_parser = HTMLParser.HTMLParser()
    texto = html_parser.unescape(texto.decode("utf8", errors="ignore"))
    textouni = unicode(texto)
    texto = unicodedata.normalize('NFKD', textouni).encode('ascii', 'ignore')

    if texto.strip():
        return inicioDado + texto + fimDado
    else:
        return texto


def escreve_data(texto):
    # Deixa apenas maiusculas
    texto = texto.strip().strip(".").strip(";").upper().replace("\n", " ").replace("\r", " ").replace("    ", " ")
    # Verifica se a data está no formato d{d} mmm{mmmm} aa{aa}
    m = re.search("^(\d{1,2}\s+)?(\w){3,15}\s+(\d){2,4}$", texto)
    if m:
        temp = texto.split(" ")
        if (len(temp) > 2):
            texto = temp[0] + " " + temp[1][:3] + " " + temp[2]
        else:
            texto = temp[0][:3] + " " + temp[1]
    else:
        # Verifica se a data está no formato mmm{mmmmm}.{-mmm{mmmmm}.} aa{aa}
        m = re.search("^(\w){3,15}\.(-(\w){3,15}\.)?\s(\d){2,4}$", texto)
        if m:
            temp = texto.split(".")
            texto = temp[0][:3] + temp[len(temp) - 1]
        else:
            # Verifica se a data está no formato mmm{mmmmm} d{d}{,} aa{aa}
            m = re.search("^(\w){3,15}(\s|\s{,4})(\d){1,2},?\s(\d){2,4}$", texto)
            if m:
                temp = texto.replace("  ", " ").replace("  ", " ").replace("  ", " ").split(" ")
                texto = temp[1].strip(",") + " " + temp[0][:3] + " " + temp[2]
            else:
                # Verifica se a data está no formato mmm{mmmmm}, aa{aa}
                m = re.search("^(\w){3,15},\s(\d){2,4}$", texto)
                if m:
                    temp = texto.split(",")
                    texto = temp[0][:3] + temp[len(temp) - 1]
                else:
                    # Verifica se a data está no formato m{m}{,} aa{aa}
                    m = re.search("^(\d){1,2},?\s(\d){2,4}$", texto)
                    # Como o índice é começa de zero, criei uma posição vazia para simplificar os tratamentos abaixo
                    meses = ["", "JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"]
                    
                    if m:
                        temp = texto.split(",")
                        texto = meses[int(temp[0])] + " " + temp[len(temp) - 1]
                    else:
                        # Verifica se a data está no formato mmm{mmm}-mmm{mmm}{.|,} aa{aa}
                        m = re.search("^(\w){3,15}-(\w){3,15}[,.]?\s(\d){2,4}$", texto)
                        if m:
                            temp = texto.split(" ")
                            texto = temp[0][:3] + " " + temp[len(temp) - 1]
                        else:
                            # Verifica se a data está no formato aaaa{ |-|/}d{d}{ |-|/}d{d}
                            m = re.search("^(\d){4}(-| |/)(\d){2}(-| |/)(\d){2}$", texto)
                            if m:
                                temp = texto.replace(" ", "/").replace("-", "/").split("/")
                                texto = temp[2] + " " + meses[int(temp[1])] + " " + temp[0]
                            else:
                                # Verifica se a data está no formato aaaa{ |-|/}mmm{mmmm}{ |-|/}d{d}
                                m = re.search("^(\d){4}(-| |/)(\w){3,15}(-| |/)(\d){2}$", texto)
                                if m:
                                    temp = texto.replace(" ", "/").replace("-", "/").split("/")
                                    texto = temp[2] + " " + temp[1] + " " + temp[0]
                                else:
                                    # Verifica se a data está no formato aa{aa} mmm{mmm} (d{d})
                                    m = re.search("^(\d){2,4}\s(\w){3,15}(\s(\d){1,2})?$", texto)
                                    if m:
                                        temp = texto.split(" ")
                                        texto = temp[1][:3] + " " + temp[0]
                                        if len(temp) == 3:
                                            texto = temp[2] + " " + texto
                                    else:
                                        # Verifica se a data está no formato d{d}-d{d} mmm{mmm}{.} aa{aa}
                                        m = re.search("^(\d){1,2}-\d{1,2}\s\w{3,15}.?\s\d{2,4}$", texto)
                                        if m:
                                            temp = texto.split(" ")
                                            texto = temp[0][:2] + " " + temp[1][:3] + " " + temp[2]
                                        else:
                                            # Verifica se a data está no formato mmm{mmm}/mmm{mmm} aa{aa}
                                            m = re.search("^\w{3,15}/\w{3,15}\s\d{2,4}$", texto)
                                            if m:
                                                temp = texto.split("/")
                                                texto = temp[0][:3] + " " + temp[1].split(" ")[0] + " " + \
                                                        temp[1].split(" ")[1]
                                            else:
                                                # Verifica se a data está no formato mmm{mmm} + mmm{mmm} aa{aa}
                                                m = re.search("^\w{3,15}\s\+\s\w{3,15}\s\d{2,4}$", texto)
                                                if m:
                                                    temp = texto.split("+")
                                                    texto = temp[0][:3] + " " + temp[1].split(" ")[0] + " " + \
                                                            temp[1].split(" ")[1]


    # tradução do inglês
    texto = texto.replace("FEB", "FEV").replace("APR", "ABR").replace("MAY", "MAI").replace("AUG", "AGO")
    texto = texto.replace("SEP", "SET").replace("OCT", "OUT").replace("DEC", "DEZ")

    return escreve_dado(texto)


blob_service = BlobService(account_name='testethiago', account_key='GsPRxLnWF/OKXMR8h0LMPdic1ndc2HSHoJUTjDOqZgNRXH2Mc3bSUNoy5z/yoXHHCooXD6/TzJMwFEsnTjVEyQ==')

# Parametros
execucaoNaNuvem = False

if execucaoNaNuvem:
    caminhoArquivoExpressoes = "expressoes.csv"
    listaArtigos = blob_service.list_blobs('doi') 
else:
    caminhoArquivoExpressoes = "d:\\expressoes.csv"
    listaArtigos = os.listdir("d:\\doi")

tamanhoCodSite = 6
o = AnalisadorFabric()
separador = ";"
# Fim parametros

qtdeartigos = 0


conteudoProcessamento = ""
conteudoProcessamento = conteudoProcessamento + escreve_dado("Doi")
conteudoProcessamento = conteudoProcessamento + separador
conteudoProcessamento = conteudoProcessamento + escreve_dado("Titulo")
conteudoProcessamento = conteudoProcessamento + separador
conteudoProcessamento = conteudoProcessamento + escreve_dado("Autores")
conteudoProcessamento = conteudoProcessamento + separador
conteudoProcessamento = conteudoProcessamento + escreve_dado("Publicacao")
conteudoProcessamento = conteudoProcessamento + separador
conteudoProcessamento = conteudoProcessamento + escreve_dado("Submissao")
conteudoProcessamento = conteudoProcessamento + separador
conteudoProcessamento = conteudoProcessamento + escreve_dado("Aceite")
conteudoProcessamento = conteudoProcessamento + "\n"

for artigo in listaArtigos:
    if execucaoNaNuvem:
        nomeArtigo = artigo.name
    else:
        nomeArtigo = artigo

    if nomeArtigo.endswith(".htm"):
        if "101016jajodo200802018" in nomeArtigo:
            artigo = "101016jajodo200802018.htm"
        doi = os.path.splitext(nomeArtigo)[0]
        r = o.criar_analisador(doi)

        if execucaoNaNuvem:
            resultado = r.analisar_arquivo(doi, nomeArtigo)
        else:
            resultado = r.analisar_arquivo(doi, 'd:\\doi\\' + nomeArtigo)
        # escrever arquivo
        conteudoProcessamento = conteudoProcessamento + escreve_dado(doi)
        conteudoProcessamento = conteudoProcessamento + separador
        conteudoProcessamento = conteudoProcessamento + escreve_dado(resultado.titulo)
        conteudoProcessamento = conteudoProcessamento + separador
        conteudoProcessamento = conteudoProcessamento + escreve_dado("|".join(([str(x) for x in resultado.autores])))
        conteudoProcessamento = conteudoProcessamento + separador
        conteudoProcessamento = conteudoProcessamento + escreve_data(str(resultado.dataPublicacao))
        conteudoProcessamento = conteudoProcessamento + separador
        conteudoProcessamento = conteudoProcessamento + escreve_data(str(resultado.dataSubmissao))
        conteudoProcessamento = conteudoProcessamento + separador
        conteudoProcessamento = conteudoProcessamento + escreve_data(str(resultado.dataAceite))
        conteudoProcessamento = conteudoProcessamento + "\n"

        qtdeartigos += 1
        print 'Artigos processados:', str(qtdeartigos)
next

if execucaoNaNuvem:
    blob_service.put_blob('consolidado', 'processamento_doi', conteudoProcessamento)
else:
    # criar arquivo de saida
    f = open("d:\\processamento_doi.csv", 'w')
    f.write(conteudoProcessamento)
    f.close() 

# print resultado
print 'fim'