# -*- coding: ISO-8859-1 -*-
"""
Random graph from given degree sequence.
Draw degree rank plot and graph with matplotlib.
"""
import os.path
from networkLib import *
import pandas as pd
import networkx as nx
import sys
import logging
reload(sys)

# Configuracoes utilizadas
diretorioBase = "F:\\Hier-Lat-CNPQ\\"

def gerar_csv(nl, arquivoStats):
    if not (nl.verifica_arquivo_existe(nl.nomeArquivoPares)):
        nl.exibir_andamento("Iniciando processamento lattes", logging.DEBUG)
        nl.processar_lattes()
    else:
        nl.exibir_andamento("Arquivo lattes ja existente: " + nl.nomeArquivoPares, logging.DEBUG)

    total_nos = len(nl.obter_arquivo(nl.nomeArquivoIDs).split('\n'))  # todo container

    statinfo = os.stat(nl.nomeArquivoPares)
    
    if statinfo.st_size > 0 and not (os.path.isfile(arquivoStats)): #

        arqPassado = nl.obter_arquivo(nl.nomeArquivoPares)  # todo container
        passado = pd.read_csv(StringIO(arqPassado), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)
        amplo = nx.from_pandas_dataframe(passado, 'a', 'b')
        G = nx.Graph(amplo)
        if G.number_of_edges() > 1:
            nl.exibir_andamento("Imprimir stats comecando", logging.DEBUG)
            try:
                nl.imprimir_stats(G, total_nos, os.path.join(arquivoStats))
            except Exception as e:
                nl.exibir_andamento('Erro ao gerar stats - ' + e.message + ' - ' +
                                    nl.nomeArquivoPares, logging.ERROR)
        else:
            nl.exibir_andamento('Rede com poucos nos', logging.WARNING)

    else:
        f = open(os.path.dirname(nl.nomeArquivoIDs) + '\\tamanho.txt', 'w')
        f.write(str(total_nos))
        f.close()
        nl.exibir_andamento('Arquivo vazio ou ja criado - ' + arquivoStats, logging.WARNING)
    
    nl.exibir_andamento('fim ')

# Gera um CSV em cada pasta das hierarquias do lattes
def gerar_csvs():
    global diretorioBase
    nl = NetworkLib(
    TipoArmazenamento.local, TipoToken.kid,
    "evallattes", nivelLog=logging.INFO, forcarGravacao=False, frequenciaStatus=2000)

    nl.gerarOctave = False  # Indica se deve gerar o arquivo no formato Octave
    nl.gerarPares = True  # Indica se deve gerar o arquito no formato de pares
    nl.AnoInicial = 2012
    nl.AnoFinal = 2014
    nl.validarMatrizDoisSentidos = False #True
    nl.nomePastaSaida = "{0}\\"

    for root, directories, filenames in os.walk(diretorioBase.decode("utf-8")):
        for filename in filenames:
            if (filename == "ids.txt"):
                nl.exibir_andamento("Encontrou ids.txt", logging.INFO)
                nl.exibir_andamento(root, logging.DEBUG)

                nl.nomeArquivoIDs = os.path.join(root, filename)
                nl.nomeArquivoPares = os.path.join(root, 'pair-{0}-{1}.csv'.format(nl.AnoInicial, nl.AnoFinal))
                arquivoStats = os.path.join(root, 'stats-{0}-{1}.csv'.format(nl.AnoInicial, nl.AnoFinal))
                nl.nomeLogErros = os.path.join(root, 'erros.csv')

                gerar_csv(nl, arquivoStats)
            else:
                nl.exibir_andamento('Arquivo diferente ' + filename, logging.DEBUG)

    nl.exibir_andamento('fim ')
    #nl.integrar_csvs(diretorioBase, "stats_completa.csv", 2011, 2015, "Doutores Lattes")

gerar_csvs()

