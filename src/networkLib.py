# -*- coding: utf-8 -*-
import codecs
import datetime
import mysql.connector
import uuid
import random
from operator import itemgetter
import xml.etree.ElementTree as ET
import os
import time
import re
import sys
import unicodedata
import pandas as pd
import numpy as np
import networkx as nx
import scipy as sc
import requests
import string
from sets import Set
from cStringIO import StringIO
import shutil
from requests.packages.urllib3.connection import ConnectionError
from enum import Enum
import logging
from azure.storage.blob import BlockBlobService

##################################################################
# Enumeracoes de parametros
##################################################################
class TipoArmazenamento(Enum):
    local = 1
    azureblob = 2
    awss3 = 3
    googlestorage = 4

class TipoToken(Enum):
    kid = 1,
    cnpqid = 2

class TipoContainer(Enum):
    idsets = 1,
    lattesextraido = 2
    results = 3
    pdfs = 4
    dois = 5

##################################################################
# Configuracoes padrao do processo
##################################################################

class NetworkLib(object):
    # Definicoes de anos dos trabalhos
    AnoInicial = 2010
    AnoFinal = 2015
    agruparAnos = True # indica se ira gerar apenas um arquivo com todos os anos (senao gera um arquivo para cada ano)

    accountName = ''
    accountKey = ''
    gerarDOI = False  # Indica se deve buscar as informacoes dos DOIs
    gerarOctave = False  # Indica se deve gerar o arquivo no formato Octave
    gerarGephi = False  # Indica se deve gerar o arquito no formato do Gephi
    gerarPares = True  # Indica se deve gerar o arquito no formato de pares
    gerarPandas = False  # Indica se deve gerar o arquito no formato do Pandas
    gerarPDFdoHTML = False  # Indica se deve buscar as informacoes dos DOIs
    gerarPDFdoLattes = False  # Indica se deve buscar as informacoes dos DOIs

    tentativasAleatorio = 1000

    forcarGravacao = False # Indica se deve sobrescrever arquivos ja existentes

    usarProxy = False  # Indica se utilizara proxy para as requisicoes DOI
    proxyRequisicao = {"http": "http://localhost:8888/"}

    nomePastaCurriculos = ''  # Pasta onde estao os XMLs com os lattes
    nomePastaTokens = ""
    
    nomePastaDOI = ''  # Pasta onde ficarao os htmls dos DOIs
    nomeArquivoResumoDOI = ''
    nomeArquivoGephi = '' #Anos[0]+'-'+Anos[len(Anos)-1]
    nomeArquivoPares = ''
    nomeArquivoOctave = ''
    nomeArquivoPandas = ''
    nomePadraoArquivo = "{0}pair-{1}-{2}.csv"
    nomePadraoDiff = "{0}pair-{1}-{2}-dif-{3}-{4}.csv"
    complementoSaida = ""

    nomePastaPDFLattes = "d:\\pdfl\\"  # Pasta onde ficarao os htmls dos DOIs
    nomePastaPDFDOI = "d:\\pdfd\\"  # Pasta onde ficarao os htmls dos DOIs

    nomeLogErros = ''

    validarMatrizDoisSentidos = True  # Indica se a coautoria deve ser citada por ambos autores
    rangeAnos = 0

    separadorPastas = "//"
    utilizarArtigos = True
    utilizarEventos = False
    utilizarLivros = False
    utilizarJornaisRevistas = False
    utilizarProducoesTecnicas = False
    utilizarOrientacaoConcluida = False
    tamanhoMaximoProcessar = 0

    # Inicializacao
    def __init__(self, armazenamento = TipoArmazenamento.azureblob, tipoToken = TipoToken.kid, experimento="teste",
                 forcarGravacao = True, nivelLog = logging.DEBUG, frequenciaStatus = 2000, accountName='', accountKey='',
                 utilizarArtigos = True, utilizarEventos = False, utilizarLivros = False, utilizarJornaisRevistas = False,
                 utilizarProducoesTecnicas = False, utilizarOrientacaoConcluida = False,
                 reaproveitaInicio=''):
        self.utilizarArtigos = utilizarArtigos
        self.utilizarEventos = utilizarEventos
        self.utilizarLivros = utilizarLivros
        self.utilizarJornaisRevistas = utilizarJornaisRevistas
        self.utilizarProducoesTecnicas = utilizarProducoesTecnicas
        self.utilizarOrientacaoConcluida = utilizarOrientacaoConcluida
        self.accountName = accountName
        self.accountKey = accountKey
        self.frequenciaStatus = frequenciaStatus # A cada quantos autores processados sera mostrada uma informaçao de status
        self.forcarGravacao = forcarGravacao
        self.converterToken = (tipoToken == TipoToken.kid)  # Necessario converter quando a lista de IDS esta no formato K000000000
        self.Lattes = [] # lista que guardara os curriculos
        self.listaSaidas = []
        self.semPubs = 0 # contador utilizado para log
        self.totalProcessados = 0 # contador utilizado para log
        self.tipoArmazenamento = armazenamento
        if reaproveitaInicio == '':
            self.inicio_experimento = datetime.datetime.now().strftime("%Y%b%d") #-%H%M
        else:
            self.inicio_experimento = reaproveitaInicio
        self.experimento = experimento
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            self.block_blob_service = BlockBlobService(account_name=self.accountName,
                                                  account_key=self.accountKey)
            self.nomePastaCurriculos = 'cvs/'  # Pasta onde estao os XMLs com os lattes
            self.nomePastaTokens = "tokens/"
            self.nomePastaDOI = 'html/'  # Pasta onde ficarao os htmls dos DOIs
            self.nomeArquivoResumoDOI = 'resumo/'
            self.nomePastaSaida = self.experimento + '/' + self.inicio_experimento + '/{0}/'
            self.nomeArquivoGephi = self.nomePastaSaida + 'network-{1}.csv'  # Anos[0]+'-'+Anos[len(Anos)-1]
            self.nomeArquivoPares = self.nomePastaSaida + 'pair-{1}.csv'
            self.nomeArquivoOctave = self.nomePastaSaida + 'octave-{1}.mat'
            self.nomeArquivoPandas = self.nomePastaSaida + 'pandas-{1}.mat'
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            self.separadorPastas = "//"
            self.nomePastaCurriculos = 'D:\\lattescaptcha\\Extraido\\'  # Pasta onde estao os XMLs com os lattes
            self.nomePastaTokens = "D:\\lattescaptcha\\basecompleta\\tokens\\"
            self.nomePastaSaida = 'f:\\bd\\Sel\\douglas\\{0}\\'
            self.nomePastaDOI = self.nomePastaSaida + 'doi\\'  # Pasta onde ficarao os htmls dos DOIs
            self.nomeArquivoResumoDOI = self.nomePastaSaida + 'arqdoi-{1}.csv'
            self.nomeArquivoGephi = self.nomePastaSaida + 'network-{1}.csv'  # Anos[0]+'-'+Anos[len(Anos)-1]
            self.nomeArquivoPares = self.nomePastaSaida + 'pair-{1}.csv'
            self.nomeArquivoOctave = self.nomePastaSaida + 'octave-{1}.mat'
            self.nomeArquivoPandas = self.nomePastaSaida + 'pandas-{1}.mat'

        # Logging config
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(self.inicio_experimento + '.log')
        fh.setLevel(logging.INFO)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(nivelLog)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        # add the handlers to the logger
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    # Retorna a pasta utilizada para os arquivos de resultados com a formataçao da area utilizada
    def obter_saida_formatada(self):
        if self.complementoSaida == "":
            criterio = self.nomeArea
        else:
            criterio = self.nomeArea + '/' + self.complementoSaida

        return self.nomePastaSaida.format(criterio)

    # Lista pasta ou container
    def listar(self, containerBase):
        if self.tipoArmazenamento == TipoArmazenamento.local:
            return os.walk(containerBase)
        else:
            return self.block_blob_service.list_blobs(containerBase)

    # Verifica se o arquivo existe no modo local ou nuvem
    def verifica_arquivo_existe(self, caminho_completo_arquivo, container = TipoContainer.lattesextraido):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            try:
                if (isinstance(container, basestring)):
                    containerName = container
                else:
                    containerName = container.name

                retorno = self.block_blob_service.get_blob_properties(containerName, caminho_completo_arquivo)
                return True
            except:
                return False
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            return os.path.exists(caminho_completo_arquivo)
        else:
            return False # exception

    # Inicia gravacao de arquivo no modo local ou nuvem para appends
    def iniciar_gravar_arquivo(self, caminho_completo_arquivo, container = TipoContainer.lattesextraido):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            self.arquivo_temp = StringIO()
            self.container_temp = container
            self.caminho_temp = caminho_completo_arquivo
            return
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            self.arquivo_temp = open(caminho_completo_arquivo, 'w')
            return
        else:
            return  # exception

    # Dar append no arquivo no modo local ou nuvem
    def continuar_gravar_arquivo(self, conteudo):
        self.arquivo_temp.write(conteudo)

    # Fechar o arquivo no modo local ou nuvem
    def encerrar_gravar_arquivo(self):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            self.block_blob_service.create_blob_from_text(self.container_temp.name, self.caminho_temp,
                                                          self.arquivo_temp.getvalue())
            return
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            self.arquivo_temp.close()
            return
        else:
            return  # exception

    # Abrir pandas em arquivo no modo local ou nuvem
    def abrir_arquivo_pandas(self, caminho_completo_arquivo,
                              container=TipoContainer.lattesextraido, sep=';', header=0, index=0):
        self.exibir_andamento("Abrir pandas: " + caminho_completo_arquivo, logging.DEBUG)

        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            if (isinstance(container, basestring)):
                containerName = container
            else:
                containerName = container.name

            arqTemp = StringIO()
            arqTemp.write(self.block_blob_service.get_blob_to_text(containerName, caminho_completo_arquivo).content)
            arqTemp.seek(0)
            return pd.read_csv(arqTemp, sep=sep, header=header, index_col=index)
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            return pd.read_csv(caminho_completo_arquivo, sep=sep, header=header, index_col=index)
        else:
            return  # exception

    # Gravar pandas em arquivo no modo local ou nuvem
    def gravar_arquivo_pandas(self, caminho_completo_arquivo, pandas,
                       container=TipoContainer.lattesextraido, sep = ';', header=True, index=True):
        self.exibir_andamento("Gravar saida: " + caminho_completo_arquivo, logging.DEBUG)

        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            if (isinstance(container, basestring)):
                containerName = container
            else:
                containerName = container.name

            arqTemp = StringIO()
            pandas.to_csv(arqTemp, sep=sep, header=header, index=index)
            self.block_blob_service.create_blob_from_text(containerName, caminho_completo_arquivo,
                                                          arqTemp.getvalue())
            return
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            pandas.to_csv(caminho_completo_arquivo, sep=sep, header=header, index=index)
            return
        else:
            return  # exception

    # Gravar arquivo no modo local ou nuvem
    def gravar_arquivo(self, caminho_completo_arquivo, conteudo_arquivo, container=TipoContainer.lattesextraido):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            if (isinstance(container, basestring)):
                containerName = container
            else:
                containerName = container.name

            self.block_blob_service.create_blob_from_text(containerName, caminho_completo_arquivo,
                                                          conteudo_arquivo)
            return
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            with open(caminho_completo_arquivo, 'w') as fd:
                if type(conteudo_arquivo) is str:
                    fd.write(conteudo_arquivo)
                else:
                    conteudo_arquivo.seek(0)
                    shutil.copyfileobj(conteudo_arquivo, fd)

            return
        else:
            return  # exception

    # Gravar arquivo no modo local ou nuvem
    def gravar_arquivo_se_nao_existe(self, caminho_completo_arquivo, conteudo_arquivo, container=TipoContainer.lattesextraido):
        if self.forcarGravacao or not (self.verifica_arquivo_existe(caminho_completo_arquivo, container)):
            self.gravar_arquivo(caminho_completo_arquivo, conteudo_arquivo, container)
        else:
            self.exibir_andamento("Arquivo ja gerado: " + caminho_completo_arquivo)

    # Obter arquivo no modo local ou nuvem
    def obter_arquivo(self, caminho_completo_arquivo, container = TipoContainer.lattesextraido, encoding = 'ISO-8859-1'):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            if (isinstance(container, basestring)):
                containerName = container
            else:
                containerName = container.name
            try:
                retorno = self.block_blob_service.get_blob_to_text(containerName, caminho_completo_arquivo, encoding=encoding)
            except Exception as e:
                self.exibir_andamento('Erro ao obter arquivo: ' + str(e) + ' - '+ caminho_completo_arquivo)
            if retorno:
                return retorno.content
        elif self.tipoArmazenamento == TipoArmazenamento.local:
            f = codecs.open(caminho_completo_arquivo, 'r', 'ISO-8859-1')
            retorno = "".join(f.readlines())
            f.close()
            return retorno
        else:
            return   # exception

    def expand_list(self, original_list, total_len):
        for i in range(len(original_list), total_len):
            original_list.append("")
        return original_list

    def nomes_lattes(self, codigos):
        for c in codigos:
            nome = ''
            for r, d, f in os.walk(u'F:\\Hier-Lat-CNPQ\\nomes\\'):
                for dir in d:
                    if c <> "" and c in dir:
                        reg = re.findall(r"[A\d]{8}.-.(.*?)\\", dir + "\\")
                        if len(reg)>0:
                            nome = self.limpar_caracteres(re.findall(r"[A\d]{8}.-.(.*?)\\", dir + "\\")[0])
            yield nome

    # Integra csvs com estatisticas da caracterizacao do lattes ou mag
    def integrar_csv(self, ano_inicio, ano_fim, filename, tipo_pub, csvIntegrado, root=''):
        if ("stats-" + str(ano_inicio) + "-" + str(ano_fim) + ".csv" in filename):
            if ("attes" in tipo_pub):  # Lattes
                codigos = re.findall(r"([A\d]{8})", root)
                codigos = self.expand_list(codigos, 4)
                nomes = list(self.nomes_lattes(codigos)) #re.findall(r"[A\d]{8}.-.(.*?)\\", root + "\\")
                nomes = self.expand_list(nomes, 4)

                linhas = self.obter_arquivo(os.path.join(root, filename)).split('\n')

                #temp
                #t= open(os.path.join(root, 'tamanho.txt'))
                #tamanhoTotal = t.readline()
                #t.close()

                if len(linhas) > 1:
                    l = linhas[len(linhas)-1].replace(";", "|")
                    #l = l[0: l.index('|') + 1] + tamanhoTotal + l[l.index('|', l.index('|') +1):]
                    csvIntegrado.write(("{0}| {1}| {2}| {3}| {4}| {5}| {6}| {7}| {8}| \n").format(
                        codigos[0], nomes[0], codigos[1], nomes[1], codigos[2], nomes[2], codigos[3],
                        nomes[3], l, tipo_pub))
                    self.exibir_andamento(root, logging.DEBUG)
            else:  # MAG
                codigos = re.findall(r"([\w\d]{8})", root)

                linhas = self.obter_arquivo(os.path.join(root, filename)).split('\n')

                if len(linhas) > 1:
                    codigoArea = codigos[len(codigos) - 1]
                    nomeAreaMAG = self.buscar_nome_revista_mag(codigoArea)
                    linha = ("{0}| {1}| {2}| {3}| {4}| {5}| {6}| {7}| {8}| {9} \n").format(
                        codigoArea, nomeAreaMAG, "", "", "", "", "", "", linhas[len(linhas)-1].replace(";", "|"), tipo_pub)
                    csvIntegrado.write(linha)

    # Integra os csvs de resultados
    # localonly
    def integrar_results(self, diretorio_base):
        arqSaida = open(os.path.join(diretorio_base, 'results_geral.csv'), 'w')
        for root, directories, files in os.walk(diretorio_base):
            for file in files:
                if not 'results_geral' in file and 'results' in file:
                    nomeArqEntrada = os.path.join(root, file)
                    nomeArqStats = os.path.join(root, 'stats-{0}.csv'.format(file[8:17]))
                    arqEntrada = open(nomeArqEntrada)
                    stats = '<error>'
                    try:
                        arqStats = open(nomeArqStats)
                        linhas = arqStats.readlines()
                        stats = linhas[len(linhas)-1]
                        arqStats.close()
                    except:
                        self.exibir_andamento('Sem arquivo ' + nomeArqStats, logging.ERROR)
                    arqSaida.write(arqEntrada.readlines()[1] + ';' + stats + ';'+root+  ';' + file + '\n')
                    arqEntrada.close()
                    self.exibir_andamento(nomeArqEntrada)
        arqSaida.close()

    def buscar_nome_revista_mag(self, codigo):
        try:
            cnx = mysql.connector.connect(user='pythonuser', password='pythonuser', host='127.0.0.1',
                                      database='pesquisa', raise_on_warnings=False)
            cursor = cnx.cursor()

            self.exibir_andamento('Buscando nome da publicacao', logging.DEBUG)

            # aqui
            query = ("select journal_name from journals where journal_id = '" + codigo + "'")
            cursor.execute(query)
            row = cursor.fetchone()
            return self.limpar_caracteres(row[0])
        finally:
            cnx.close()

    def integrar_csvs(self, diretorioBase, arq_completo, ano_inicio, ano_fim, tipo_pub):
        csvIntegrado = StringIO()
        for root, directories, filenames in os.walk(diretorioBase):
            for filename in filenames:
                self.integrar_csv(ano_inicio, ano_fim, filename, tipo_pub, csvIntegrado, root)

        self.gravar_arquivo(os.path.join(diretorioBase, arq_completo), csvIntegrado, TipoContainer.results )
        csvIntegrado.close()

    # Imprime algumas informacoes sobre a rede
    def imprimir_stats(self, grafico, qt_total, arq = ""):

        if self.verifica_arquivo_existe(arq, TipoContainer.results) and not self.forcarGravacao:
            self.exibir_andamento("Arquivo ja gerado: " + arq)
        else:
            # Tamanho da rede
            qt_nos = nx.number_of_nodes(grafico)
            # Quantidade de pares (conexoes)
            qt_pares = grafico.number_of_edges()
            # Componentes conectados
            qt_componentes_conectados = nx.number_connected_components(grafico)
            # Grau medio
            grau_medio = np.mean(nx.degree(grafico).values())
            # Densidade
            num_densidade = nx.density(grafico)
            # Clustering medio
            try:
                num_clustering_zeros = nx.average_clustering(grafico, count_zeros=False)
            except Exception as e:
                num_clustering_zeros = 0
            try:
                num_clustering = nx.average_clustering(grafico)
            except Exception as e:
                num_clustering = 0

            diametro_maximo = 0.0
            parcial = 0.0
            quantidade = 0.0
            media = 0.0
            self.exibir_andamento('Iniciando analise de sub-redes', logging.INFO)
            for sub_rede in nx.connected_component_subgraphs(grafico):
                self.exibir_andamento('Diametro da sub-rede', logging.DEBUG)
                diametro = nx.diameter(sub_rede)

                # 'Verifica se e o maior diametro ate aqui'
                if diametro > diametro_maximo:
                    diametro_maximo = diametro

                # 'Comprimento medio - Nao considerava segmentos de tamanho 1 - agora esta considerando'
                if (len(sub_rede)):
                    quantidade = quantidade + 1
                    parcial = parcial + nx.average_shortest_path_length(sub_rede)
                self.exibir_andamento('Sub-rede:' + str(quantidade) + ' de ' + str(qt_componentes_conectados), logging.DEBUG)

            if quantidade>0:
                comprimento_medio = parcial / quantidade
            else:
                comprimento_medio = 0

            self.exibir_andamento('Tamanho da rede (n):'+ str(qt_nos), logging.DEBUG)
            self.exibir_andamento('Tamanho total (inclui nos sem pares):'+ str(qt_total), logging.DEBUG)
            self.exibir_andamento('Pares (conexoes):'+ str(qt_pares), logging.DEBUG)
            self.exibir_andamento('Componentes conectados:'+ str(qt_componentes_conectados), logging.DEBUG)
            self.exibir_andamento('Grau medio:'+ str(grau_medio), logging.DEBUG)
            self.exibir_andamento('Densidade (x1000):'+ str(num_densidade * 1000), logging.DEBUG)
            self.exibir_andamento('Clustering medio zeros:'+ str(num_clustering_zeros), logging.DEBUG)
            self.exibir_andamento('Clustering medio:'+ str(num_clustering), logging.DEBUG)
            self.exibir_andamento('Diametro de rede:'+ str(diametro_maximo), logging.DEBUG)
            self.exibir_andamento('Comprimento medio:'+ str(comprimento_medio), logging.DEBUG)  # nx.average_shortest_path_length(G)

            # Verifica se deve ser gerado um arquivo com o resultado
            if arq <> "":
                cabecalho = "Tamanho da rede (n); Tamanho total (inclui nos sem pares); Pares (conexoes); Componentes conectados; Grau medio; Densidade (x 1000); Clustering medio zeros; Clustering medio; Diametro de rede; Comprimento medio"
                conteudo = "{0}; {1}; {2}; {3}; {4}; {5}; {6}; {7}; {8}; {9}".format(qt_nos, qt_total, qt_pares, qt_componentes_conectados, grau_medio, num_densidade * 1000, num_clustering_zeros, num_clustering, diametro_maximo, comprimento_medio)
                self.exibir_andamento(cabecalho)
                self.exibir_andamento(conteudo)
                self.gravar_arquivo(arq, cabecalho + "\n" + conteudo, TipoContainer.results)

    def nova_lista(self, it):
        indice = 0
        for a, b, c in it:
            if indice % 1000000 == 0 and indice > 0:
                self.exibir_andamento('Filtrando lista: ' + str(indice), logging.DEBUG)
            indice = indice + 1
            if c>0:
                yield a, b, c

    # Imprime os primeiros registros de uma lista
    def ordenar_resultados(self, titulo, quantidade, lista):
        if titulo <> 'Random':
            self.exibir_andamento('Calculando '+ titulo, logging.DEBUG)
        novalLista = self.nova_lista(lista)

        for a, b, c in sorted(novalLista, key=itemgetter(2), reverse=True):
            yield a, b, c
            if quantidade >= 0:
                yield a, b, c
                quantidade -= 1
            else:
                return

    # Aumenta o tamanho de uma rede incluindo novos nďçs soltos
    def expand_network(self, grafico, novo_tamanho):
        grafico.add_nodes_from(range(novo_tamanho-nx.number_of_nodes(grafico)))

        return grafico

    def previsao_aleatoria(self, grafico, quantidadePrevisoes):
        quantidadeRetornos = 0
        while quantidadeRetornos < quantidadePrevisoes:
            lista = grafico.nodes()
            no1 = str(random.choice(lista))
            no2 = str(random.choice(lista))
            if not (grafico.has_edge(no1, no2)):
                quantidadeRetornos = quantidadeRetornos + 1
                yield no1, no2, 1

    # Metodo
    # pasta_base: caminho completo da pasta onde estao os arquivos
    # ano_inicial: ano inicial do periodo do primeiro arquivo
    # ano_final: ano final do periodo do primeiro arquivo
    # novo_ano: ano do segundo arquivo
    def gerar_diferencial(self, container, pasta_base, ano_inicial_passado, ano_final_passado, ano_inicial_futuro, ano_final_futuro):
        nomeArquivoPassado = self.nomePadraoArquivo.format(pasta_base, ano_inicial_passado, ano_final_passado)
        nomeArquivoFuturo = self.nomePadraoArquivo.format(pasta_base,  ano_inicial_futuro, ano_final_futuro)
        nomeArquivoSaida = self.nomePadraoDiff.format(pasta_base, ano_inicial_futuro, ano_final_futuro, ano_inicial_passado, ano_final_passado)

        arquivoPassado = self.obter_arquivo(nomeArquivoPassado, container)
        arquivoFuturo = self.obter_arquivo(nomeArquivoFuturo, container)

        # Carregando os arquivos de entrada
        pdPassado = pd.read_csv(StringIO(arquivoPassado), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)
        pdFuturo = pd.read_csv(StringIO(arquivoFuturo), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)

        # Cria colunas calculadas com a concatenacao dos nomes dos pares, nas duas ordens
        pdPassado['c'] = pdPassado.a + pdPassado.b
        pdPassado['d'] = pdPassado.b + pdPassado.a

        # Cria coluna calculada com a concatenacao dos nomes dos pares
        pdFuturo['c'] = pdFuturo.a + pdFuturo.b

        # Cria uma lista com o indice das linhas em que o registro do arquivo amplo foi encontrado no restrito
        indice = pdFuturo.c.isin(pdPassado.c) | pdFuturo.c.isin(pdPassado.d)

        # Retorna apenas os registros que nao haviam sido encontrados na linha anterior
        saida = pdFuturo[~indice]

        # Apaga coluna calculada
        del saida['c']

        # exporta arquivo com a saida
        self.gravar_arquivo_pandas(nomeArquivoSaida, saida, TipoContainer.results, sep=';', header=False, index=False)

        self.exibir_andamento('CSV diferencial gerado para ' + pasta_base)

    def definir_nome_area(self):
        if self.tipoArmazenamento == TipoArmazenamento.azureblob:
            r = re.search(r"(?P<area>.*?)/.*?\.(\w){3}", self.nomeArquivoIDs)
            if r is None:
                self.nomeArea = 'semarea'
            else:
                self.nomeArea = r.group("area")
        else:
            self.nomeArea = os.path.dirname(self.nomeArquivoIDs)


    # Processo o arquivo de ids
    def processar_segmento(self, containerBase, gerarDiferencial = False, anoInicialPassado = 0, anoFinalPassado = 0):
        self.definir_nome_area()

        if "ids.txt" in self.nomeArquivoIDs:
            tmpNomeArq = self.nomePadraoArquivo.format(self.obter_saida_formatada(), self.AnoInicial, self.AnoFinal)
            self.exibir_andamento('Arquiv:' + tmpNomeArq, logging.DEBUG)
            # Verifica se o arquivo ja foi gerado
            if self.forcarGravacao or not (self.verifica_arquivo_existe(tmpNomeArq, TipoContainer.results)):
                # if not (os.path.isfile(self.nomeArquivoPares)):
                self.processar_lattes(container = containerBase)
            else: # Inclui na lista para fazer prediçoes, ja que nao passou pelo trecho que faria isso
                self.listaSaidas.append(self.obter_saida_formatada())
                self.exibir_andamento("Arquivo ja gerado: " + tmpNomeArq + " - results")

            if gerarDiferencial:
                tmpNomeArq = self.nomePadraoDiff.format(self.obter_saida_formatada(), anoInicialPassado, anoFinalPassado, self.AnoInicial, self.AnoFinal)
                if self.forcarGravacao or not (self.verifica_arquivo_existe(tmpNomeArq, TipoContainer.results)):
                    self.gerar_diferencial(TipoContainer.results, self.obter_saida_formatada(), anoInicialPassado,
                                       anoFinalPassado, self.AnoInicial, self.AnoFinal)
                else:
                    self.exibir_andamento("Arquivo ja gerado: " + tmpNomeArq + " - results")
            self.exibir_andamento('Segmento processado - id : ' + self.obter_saida_formatada(), logging.WARN)

        else:# Copia o arquivo encontrado para a saida
            # Copiar apenas arquivos do ano processado ou qualquer um se o ano for 0 (todos)
            if "{0}-{1}".format(anoInicialPassado, anoFinalPassado) in self.nomeArquivoIDs or anoInicialPassado == 0:
                conteudoArq = self.obter_arquivo(self.nomeArquivoIDs, containerBase)
                nomeArq = "{0}{1}".format(self.obter_saida_formatada(), os.path.basename(self.nomeArquivoIDs))
                self.exibir_andamento("Copiando arquivo da origem: " + self.nomeArquivoIDs + " - " + nomeArq)
                self.gravar_arquivo_se_nao_existe(nomeArq, conteudoArq, TipoContainer.results)

                # Para bases lattes, gerar dinamicamente o arquivo de ids
                nomeArqId = "{0}{1}".format(self.obter_saida_formatada(), "ids_utilizados.txt")
                if "pair" in self.nomeArquivoIDs and (not self.verifica_arquivo_existe(nomeArqId, TipoContainer.results) or self.forcarGravacao):
                    self.exibir_andamento("Gerando arquivo de ids pra base: " + self.obter_saida_formatada())
                    self.gravar_arquivo(nomeArqId, self.montar_lista_nos_de_lista_pares(conteudoArq), TipoContainer.results)
                else:
                    self.exibir_andamento("Nao precisou de id " + self.nomeArquivoIDs, logging.DEBUG)

                # Marca pasta para prediçao
                if not self.obter_saida_formatada() in self.listaSaidas:
                    self.listaSaidas.append(self.obter_saida_formatada())

                self.exibir_andamento('Segmento processado : ' + self.obter_saida_formatada(), logging.WARN)



    # Processa o lattes de uma lista de ids de um diretorio ou container na nuvem
    def processar_lista(self, containerBase, gerarDiferencial = False, anoInicialPassado = 0, anoFinalPassado = 0, filtro=''):
        lista = self.listar(containerBase)

        if self.tipoArmazenamento == TipoArmazenamento.local:
            for root, directories, filenames in lista:
                for filename in filenames:
                    self.nomeArquivoIDs = os.path.join(root, filename)
                    self.processar_segmento(containerBase, gerarDiferencial, anoInicialPassado, anoFinalPassado)

        else:
            for item in lista:
                self.nomeArquivoIDs = item.name
                if (filtro=='' or filtro in self.nomeArquivoIDs):
                    self.processar_segmento(containerBase, gerarDiferencial, anoInicialPassado, anoFinalPassado)

    # Gerar estatisticas de um segmento
    def gerar_stats_segmento(self, segmento, anoInicialPassado, anoFinalPassado, container=TipoContainer.results):
        nomeArqPassado = self.nomePadraoArquivo.format(segmento, anoInicialPassado, anoFinalPassado)
        nomeArqStats = "{0}stats-{1}-{2}.csv".format(segmento, anoInicialPassado, anoFinalPassado)
        arqPassado = self.obter_arquivo(nomeArqPassado, container)
        passado = pd.read_csv(StringIO(arqPassado), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)
        redePassado = nx.from_pandas_dataframe(passado, 'a', 'b')
        nomeArqIds = "{0}ids_utilizados.txt".format(segmento)
        ids = self.obter_arquivo(nomeArqIds, container)
        totalNos = len(ids.split('\n'))
        G = nx.Graph(redePassado)
        self.imprimir_stats(G, totalNos, nomeArqStats)

    # Gerar estatisticas dos segmentos processados
    def gerar_stats_lista(self, containerBase, anoInicialPassado, anoFinalPassado, filtro = ''):
        # Fazer predicao para cada pasta
        for caminho in self.listaSaidas:
            if (filtro == '' or filtro in caminho):
                self.gerar_stats_segmento(caminho, anoInicialPassado, anoFinalPassado)

    def predizer_segmento(self, segmento, anoInicialPassado, anoFinalPassado, anoInicialFuturo, anoFinalFuturo, container = TipoContainer.results):
        nomeArqPassado = self.nomePadraoArquivo.format(segmento, anoInicialPassado, anoFinalPassado)
        nomeArqIds = "{0}ids_utilizados.txt".format(segmento)
        nomeArqFuturoDiferencial = self.nomePadraoDiff.format(segmento, anoInicialFuturo, anoFinalFuturo,
                                                                            anoInicialPassado, anoFinalPassado)

        self.exibir_andamento(segmento)
        if self.verifica_arquivo_existe(nomeArqIds, container):
            # Carregamento de arquivos
            ids = self.obter_arquivo(nomeArqIds, container)

            if not self.verifica_arquivo_existe(nomeArqFuturoDiferencial, TipoContainer.results):
                self.exibir_andamento('Arq diferencial nao encontrado, sera gerado: ' + nomeArqFuturoDiferencial)
                self.gerar_diferencial(TipoContainer.results, segmento, anoInicialPassado, anoFinalPassado, anoInicialFuturo, anoFinalFuturo)
            else:
                self.exibir_andamento('Arq diferencial ja gerado', logging.DEBUG)

            arqFuturoDiferencial = self.obter_arquivo(nomeArqFuturoDiferencial, container)
            futuro = pd.read_csv(StringIO(arqFuturoDiferencial), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)

            arqPassado = self.obter_arquivo(nomeArqPassado, container)
            passado = pd.read_csv(StringIO(arqPassado), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)

            # Definindo variaveis com os tamanhos das listas
            novosLinksDif = len(futuro)
            totalNos = len(ids.split('\n'))

            if self.tamanhoMaximoProcessar>0 and ( (totalNos>self.tamanhoMaximoProcessar  and not ("latt" in nomeArqIds)) or # para lattes aceita o limite um pouco maior
                                                      (totalNos>self.tamanhoMaximoProcessar*1.4)):
                self.exibir_andamento("Base nao processada por falta de capacidade ({0},{1},{2}, {3})".format(totalNos,
                                                    self.tamanhoMaximoProcessar, self.tamanhoMaximoProcessar*1.4,
                                                    nomeArqIds), logging.WARN)
            else:
                self.exibir_andamento("Total nos: " + str(totalNos) + " tamanhoMaximoProcessar: " +
                                      str(self.tamanhoMaximoProcessar), logging.DEBUG)
                # Carregamento da rede com os nos do passado
                redePassado = nx.from_pandas_dataframe(passado, 'a', 'b')

                G = nx.Graph(redePassado)
                # self.imprimir_stats(G, totalNos, nomeArqStats)

                tentativasAleatorio = self.tentativasAleatorio
                parcial = 0.0

                # Colunas calculadas para encontrar interseçao
                futuro['c'] = futuro.a + futuro.b
                futuro['d'] = futuro.b + futuro.a

                nomeArq = os.path.join(segmento, 'cn{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                anoInicialFuturo, anoFinalFuturo))

                if self.verifica_arquivo_existe(nomeArq, TipoContainer.results) and not self.forcarGravacao:
                    self.exibir_andamento("Resultados já gerados para " + segmento)
                else:
                    if novosLinksDif == 0:
                        novosLinksDif = 1 # Para evitar as divisoes por zero
                    else:
                        self.exibir_andamento('Inicio previsao aleatoria (novos links: ' + str(novosLinksDif) + ")")
                        for i in range(tentativasAleatorio):
                            if i >= (self.frequenciaStatus) and (i % self.frequenciaStatus) == 0:
                                self.exibir_andamento("Fazendo previsoes aleatorias: "+ str(i) + " de " + str(tentativasAleatorio), logging.DEBUG)

                            retorno = self.previsao_aleatoria(G, novosLinksDif)
                            rdm = pd.DataFrame.from_records(self.ordenar_resultados('Random', novosLinksDif, retorno),
                                                            columns=['a', 'b', 'c'])
                            rdm['d'] = rdm.a + rdm.b
                            indice = rdm.d.isin(futuro.c) | rdm.d.isin(futuro.d)

                            parcial = parcial + (np.sum(indice) / float(novosLinksDif))

                            if i==1:
                                nomeArq = os.path.join(segmento, 'rdm{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                                 anoInicialFuturo, anoFinalFuturo))
                                self.gravar_arquivo_pandas(nomeArq, rdm, TipoContainer.results)


                    nomeArq = os.path.join(segmento, 'jc{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                    anoInicialFuturo, anoFinalFuturo))
                    if not self.verifica_arquivo_existe(nomeArq, TipoContainer.results):
                        self.exibir_andamento("Comecando jaccard")
                        # Gera uma lista com os pares e uma pontuaçao para a possibilidade de conectarem
                        retorno = nx.jaccard_coefficient(G)
                        jc = pd.DataFrame.from_records(self.ordenar_resultados('Jaccard', novosLinksDif, retorno), columns=['a', 'b', 'c'])
                        self.gravar_arquivo_pandas(nomeArq, jc, TipoContainer.results)
                    else:
                        self.exibir_andamento("Jaccard ja gerado")
                        jc = self.abrir_arquivo_pandas(nomeArq, TipoContainer.results)

                    jc['d'] = jc.a + jc.b  # cria a coluna d com a uniao dos nomes da coluna a e b

                    nomeArq = os.path.join(segmento, 'pa{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                    anoInicialFuturo, anoFinalFuturo))
                    if not self.verifica_arquivo_existe(nomeArq, TipoContainer.results):
                        self.exibir_andamento("Comecando PA")
                        # Gera uma lista com os pares e uma pontuaçao da possibilidade de conectarem
                        retorno = nx.preferential_attachment(G)
                        pa = pd.DataFrame.from_records(self.ordenar_resultados('preferencial attachment', novosLinksDif, retorno),
                                                       columns=['a', 'b', 'c'])
                        self.gravar_arquivo_pandas(nomeArq, pa, TipoContainer.results)
                    else:
                        self.exibir_andamento("PA ja gerado")
                        pa = self.abrir_arquivo_pandas(nomeArq, TipoContainer.results)

                    pa['d'] = pa.a + pa.b

                    nomeArq = os.path.join(segmento, 'ad{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                    anoInicialFuturo, anoFinalFuturo))
                    if not self.verifica_arquivo_existe(nomeArq, TipoContainer.results):
                        self.exibir_andamento("Comecando AA")
                        # Gera uma lista com os pares e uma pontuaçao da possibilidade de conectarem
                        retorno = nx.adamic_adar_index(G)
                        ad = pd.DataFrame.from_records(self.ordenar_resultados('adamic adar', novosLinksDif, retorno),
                                                       columns=['a', 'b', 'c'])
                        self.gravar_arquivo_pandas(nomeArq, ad, TipoContainer.results)
                    else:
                        self.exibir_andamento("AA ja gerado")
                        ad = self.abrir_arquivo_pandas(nomeArq, TipoContainer.results)

                    ad['d'] = ad.a + ad.b

                    nomeArq = os.path.join(segmento, 'cn{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                    anoInicialFuturo, anoFinalFuturo))
                    if not self.verifica_arquivo_existe(nomeArq, TipoContainer.results):
                        self.exibir_andamento("Comecando CN")

                        nx.set_node_attributes(G, 'community', 0)
                        retorno = nx.cn_soundarajan_hopcroft(G)
                        cn = pd.DataFrame.from_records(self.ordenar_resultados('Common neighboors', novosLinksDif, retorno),
                                                       columns=['a', 'b', 'c'])
                        self.gravar_arquivo_pandas(nomeArq, cn, TipoContainer.results)
                    else:
                        self.exibir_andamento("CN ja gerado")
                        cn = self.abrir_arquivo_pandas(nomeArq, TipoContainer.results)

                    cn['d'] = cn.a + cn.b

                    futuro['c'] = futuro.a + futuro.b
                    futuro['d'] = futuro.b + futuro.a

                    # Calculo dos indices de cada algoritmo
                    indice_rdm = 100 * parcial / tentativasAleatorio

                    indice_rdm_calc = 100 * novosLinksDif / sc.misc.comb(totalNos, 2)

                    indice = jc.d.isin(futuro.c) | jc.d.isin(futuro.d)
                    indice_jc = 100 * np.sum(indice) / float(novosLinksDif)

                    indice = pa.d.isin(futuro.c) | pa.d.isin(futuro.d)
                    indice_pa = 100 * np.sum(indice) / float(novosLinksDif)

                    indice = ad.d.isin(futuro.c) | ad.d.isin(futuro.d)
                    indice_ad = 100 * np.sum(indice) / float(novosLinksDif)

                    indice = cn.d.isin(futuro.c) | cn.d.isin(futuro.d)
                    indice_cn = 100 * np.sum(indice) / float(novosLinksDif)

                    nomeArqIndices = os.path.join(segmento,
                                                  'results-{0}-{1}-{2}-{3}.csv'.format(anoInicialPassado, anoFinalPassado,
                                                                                       anoInicialFuturo, anoFinalFuturo))
                    self.escrever_resultados(indice_rdm, indice_rdm_calc, indice_jc, indice_pa, indice_ad, indice_cn,
                                           nomeArqIndices)
        else:
            self.exibir_andamento("Arquivo de entrada nao encontrado " + nomeArqIds, logging.FATAL)

    def predizer_lista(self, containerBase, anoInicialPassado, anoFinalPassado, anoInicialFuturo, anoFinalFuturo):
        # Fazer predicao para cada pasta
        for caminho in self.listaSaidas:
            self.predizer_segmento(caminho, anoInicialPassado, anoFinalPassado, anoInicialFuturo, anoFinalFuturo)

    ###############################################################################
    def inicializa_lattes(self):
        self.exibir_andamento("Inicializando lattes", logging.DEBUG)
        self.Lattes = []
        self.rangeAnos = 0
        self.semPubs = 0
        if not (self.agruparAnos):
            self.rangeAnos = self.AnoFinal - self.AnoInicial

        for ano in range(self.rangeAnos + 1):
            self.Lattes.append([])

        self.definir_nome_area()
    ########################################

    # Grava os resultados das prediçoes feitas
    def escrever_resultados(self, rdm, rdm_calc, jc, pa, ad, cn, arq):
        cabecalho = "Rdm (x1000); Rdm Calc (x1000); JC; PA; AD; CN"
        conteudo = "{0}; {1}; {2}; {3}; {4}; {5}".format(rdm * 1000, rdm_calc * 1000, jc, pa, ad, cn)

        self.exibir_andamento(cabecalho + '\n' + conteudo)
        self.gravar_arquivo(arq, cabecalho + "\n" + conteudo, TipoContainer.results)

    # Funcao de log em tela do andamento
    def exibir_andamento(self, mensagem, level = logging.INFO):
        self.logger.log(level, mensagem)

    # Funcao para remover acentuacao, espacoes, etc
    def limpa_autor(self, autor):
        autor = self.limpar_caracteres(autor)
        # autor.encode('ascii','ignore') #forma antiga
        lixo = [',', ' ', '.', '`', '?', '-', '\\', '/', ':', '|', '\t', '\n', '*']
        for string in lixo:
            autor = autor.replace(string, '')
        if '(' in autor:
            autor = autor[:autor.index('(')]
        autor = autor.lower()

        return autor

    # Retira caracteres especiais do texto
    def limpar_caracteres(self, texto):
        if type(texto) == str:
            textouni = unicode(texto, 'utf-8', errors='ignore')
        else:
            textouni = unicode(texto)
        texto = unicodedata.normalize('NFKD', textouni).encode('ascii', 'ignore').replace('\r', '')
        return texto

    # Funcao para extrair nomes do autor em citacoes
    def nomes(self, lista):
        string = lista[len(lista) - 1]
        if ';' in string:
            lista = lista[:len(lista) - 1]
            lista.append(string[:string.index(';')])
            lista.append(string[string.index(';') + 1:])
            return self.nomes(lista)
        else:
            return lista

    def extrai_coautor(self, Artigo, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores, tipo, tag,
                         tagAno):
        dadosbasicos = Artigo.find('DADOS-BASICOS-' + tag)
        Ano = 0
        try:
            if not(dadosbasicos is None):
                if str(dadosbasicos.attrib['ANO' + tagAno])!="":
                    Ano = int(str(dadosbasicos.attrib['ANO' + tagAno]))
                else:
                    self.exibir_andamento('Ano vazio:' + str(dadosbasicos.attrib['ANO' + tagAno]), logging.DEBUG)
            else:
                self.exibir_andamento('Dados basicos nao encontrados: DADOS-BASICOS-' + tag, logging.WARN)
        except Exception as e:
            self.exibir_andamento('Ano mal formatado:' + str(dadosbasicos.attrib['ANO' + tagAno]), logging.INFO)

        if Ano >= self.AnoInicial and Ano <= self.AnoFinal:
            # verifica se os links de pdfs encontrados na refer?ncia do lattes devem ser baixados
            if self.gerarPDFdoLattes:
                conteudoTag = ET.tostring(Artigo, encoding='utf8', method='xml')
                busca = re.findall("\w{3,4}://.*?\.pdf", conteudoTag)
                if busca:
                    nomeArquivoPDFLattes = "arq1.pdf"
                    if str(dadosbasicos.attrib["DOI"]):
                        self.exibir_andamento("Baixando PDF com DOI")
                        nomeArquivoPDFLattes = self.nomePastaPDFLattes + self.limpa_autor(
                            str(dadosbasicos.attrib["DOI"])) + ".pdf"
                    else:
                        self.exibir_andamento("Baixando PDF sem DOI")
                        nomeArquivoPDFLattes = self.nomePastaPDFLattes + self.limpa_autor(
                            str(dadosbasicos.attrib['TITULO-DO-' + tag])) + ".pdf"

                    if (self.verifica_arquivo_existe(nomeArquivoPDFLattes, TipoContainer.pdfs)):
                        self.exibir_andamento("PDF ja existia", logging.WARN)
                    else:
                        try:
                            self.download_file(busca[0], nomeArquivoPDFLattes)
                            self.exibir_andamento("PDF salvo")
                        except Exception as e:
                            self.exibir_andamento("Problema ao baixar pdf", logging.WARN)

            # verifica se deve apurar os dados dos DOIs
            if self.gerarDOI:
                # Verifica se existe DOI para o artigo atual
                if str(dadosbasicos.attrib["DOI"]):
                    codigoDOI = str(dadosbasicos.attrib["DOI"])
                    # Retira o URL do site do DOI
                    if "http:" in codigoDOI and "doi" in codigoDOI:
                        codigoDOI = codigoDOI[codigoDOI.index("org") + 4:]
                    else:
                        # Tratamento para um artigo especďçfico que estava cadastrado errado
                        if self.limpa_autor(codigoDOI) == "httpwwwiovsorgcontent522810":
                            codigoDOI = "10.1167/iovs.10-5876"

                    codigoDOILimpo = self.limpa_autor(codigoDOI)

                    # Guarda o DOI em uma lista que serĂĄ retornada
                    ListaDois.add(codigoDOI)
                    # Contabiliza quantos DOIs foram encontrados
                    QuantidadeComDoi += 1
                    # 'Calcula' o nome do arquivo html com os dados do DOI
                    nomearquivodoi = self.nomePastaDOI + codigoDOILimpo + ".htm"
                    # Se o arquivo ja existir na pasta, nao executara o processo novamente
                    if not (os.path.isfile(nomearquivodoi)):
                        self.exibir_andamento('esperando' + str(self.totalProcessados))
                        time.sleep(4)
                        conteudo = self.buscar_datas_doi(codigoDOI)
                        # arquivo menor que 5k verifica se ĂŠ uma tela de escolha de link (theiet)
                        if len(conteudo) < 5000:
                            m = re.findall("<a\shref=.?\"(.*?)\"><img.*?&nbsp;&nbsp;Access\sthe", conteudo)
                            if m:
                                self.exibir_andamento(tipo + " theiet: Segunda requisicao")
                                conteudo = self.buscar_datas_doi(urlCompleta=m[len(m) - 1])

                        self.exibir_andamento('fim req web')
                        try:
                            self.gravar_arquivo(nomearquivodoi, conteudo, TipoContainer.dois)
                        except:
                            self.exibir_andamento('erro na escrita do arquivo ' + nomearquivodoi)
                            time.sleep(4)
                else:
                    self.exibir_andamento(tipo + ' sem doi')
                    QuantidadeSemDoi += 1

            if len(Artigo.findall('AUTORES'))>0:
                for tagCoautor in Artigo.findall('AUTORES'):  #
                    coautor = tagCoautor.attrib['NOME-PARA-CITACAO']
                    coautor = self.nomes([coautor])[0]
                    if coautor!="" and not self.limpa_autor(coautor) in Coautores[self.get_year_index(self.AnoInicial, Ano)]:
                        Coautores[self.get_year_index(self.AnoInicial, Ano)].add(self.limpa_autor(coautor))

                    # Se houver id do coautor (link) inclui na lista tambem
                    if 'NRO-ID-CNPQ' in tagCoautor.attrib:
                        coautor = str(tagCoautor.attrib['NRO-ID-CNPQ'])

                        if coautor!="" and not self.limpa_autor(coautor) in Coautores[self.get_year_index(self.AnoInicial, Ano)]:
                            Coautores[self.get_year_index(self.AnoInicial, Ano)].add(self.limpa_autor(coautor))

            else:
                for indice in range(len(Artigo)):
                    elemento = Artigo[indice]
                    if "DETALHAMENTO" in elemento.tag:
                        if 'NUMERO-ID-CNPQ' in elemento.attrib:
                            coautor = str(elemento.attrib['NUMERO-ID-CNPQ'])
                        elif 'NRO-ID-CNPQ' in elemento.attrib:
                            coautor = str(elemento.attrib['NRO-ID-CNPQ'])
                        elif 'NUMERO-ID-ORIENTADO' in elemento.attrib:
                            coautor = str(elemento.attrib['NUMERO-ID-ORIENTADO'])
                        elif 'NUMERO-ID-ORIENTADOR' in elemento.attrib:
                            coautor = str(elemento.attrib['NUMERO-ID-ORIENTADOR'])

                        if coautor!="" and not self.limpa_autor(coautor) in Coautores[self.get_year_index(self.AnoInicial, Ano)]:
                            Coautores[self.get_year_index(self.AnoInicial, Ano)].add(self.limpa_autor(coautor))
                            coautor = ""

                        if "NOME-DO-ORIENTADO" in elemento.attrib:
                            coautor = elemento.attrib['NOME-DO-ORIENTADO']
                        elif "NOME-DO-ORIENTADOR" in elemento.attrib:
                            coautor = elemento.attrib['NOME-DO-ORIENTADO']

                        if coautor!="" and not self.limpa_autor(coautor) in Coautores[self.get_year_index(self.AnoInicial, Ano)]:
                            Coautores[self.get_year_index(self.AnoInicial, Ano)].add(self.limpa_autor(coautor))
                            coautor = ""

        else:
            self.exibir_andamento(tipo + ' recusado pelo ano: ' + str(Ano), logging.DEBUG)

    def extrai_coautores(self, ArtigosPublicados, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores, tipo, tag, tagAno):
        # Se receber um no de publicao
        if 'SEQUENCIA-PRODUCAO' in ArtigosPublicados.attrib:
            self.extrai_coautor(ArtigosPublicados, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores, tipo, tag,
                                tagAno)
        else: # ou se receber uma lista de nos
            for Artigo in ArtigosPublicados:
                self.extrai_coautor(Artigo, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores, tipo, tag,
                                tagAno)


    # Analisa um registro de publicacao de um pesquisador. Em alguns casos pode fazer uma chamada recursiva
    def extrai_publicacao(self, ArtigosPublicados, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores):
        tmpTipo = ""
        tmpTag = ""
        tmpTagAno = ""

        if ArtigosPublicados.tag == 'ARTIGOS-PUBLICADOS':
            if self.utilizarArtigos:
                self.ultimoPublicou = True
                tmpTipo = "Artigo"
                tmpTag = "DO-ARTIGO"
                tmpTagAno = "-" + tmpTag
        elif ArtigosPublicados.tag == 'TRABALHOS-EM-EVENTOS':
            if self.utilizarEventos:
                self.ultimoPublicou = True
                tmpTipo = "Evento"
                tmpTag = "DO-TRABALHO"
                tmpTagAno = "-" + tmpTag
        elif ArtigosPublicados.tag == 'LIVROS-E-CAPITULOS':  # so esse pode ter duas divisoes ou uma
            if self.utilizarLivros:
                for i in range(len(ArtigosPublicados)):
                    self.extrai_publicacao(ArtigosPublicados[i], ListaDois, QuantidadeComDoi, QuantidadeSemDoi,
                                           Coautores)
        elif ArtigosPublicados.tag == 'LIVROS-PUBLICADOS-OU-ORGANIZADOS':
            tmpTipo = "Livro"
            tmpTag = "DO-LIVRO"
            tmpTagAno = ""
        elif ArtigosPublicados.tag == 'CAPITULOS-DE-LIVROS-PUBLICADOS':
            tmpTipo = "Capitulo"
            tmpTag = "DO-CAPITULO"
            tmpTagAno = ""
        elif ArtigosPublicados.tag == 'TEXTOS-EM-JORNAIS-OU-REVISTAS':
            if self.utilizarJornaisRevistas:
                self.ultimoPublicou = True
                tmpTipo = "Jornal-Revista"
                tmpTag = "DO-TEXTO"
                tmpTagAno = "-" + tmpTag
        elif ArtigosPublicados.tag == 'ARTIGOS-ACEITOS-PARA-PUBLICACAO':
            if self.utilizarArtigos:  # utiliza o mesmo flag dos artigos normais
                self.ultimoPublicou = True
                tmpTipo = "Artigos-Aceitos"
                tmpTag = "DO-ARTIGO"
                tmpTagAno = "-" + tmpTag
                # inicio producao-tecnica
        elif ArtigosPublicados.tag == 'ORIENTACOES-CONCLUIDAS':
            if self.utilizarOrientacaoConcluida:
                for i in range(len(ArtigosPublicados)):
                    self.extrai_publicacao(ArtigosPublicados[i], ListaDois, QuantidadeComDoi, QuantidadeSemDoi,
                                       Coautores)
        elif ArtigosPublicados.tag in '|ORIENTACOES-CONCLUIDAS-PARA-MESTRADO|ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO|' + \
                'ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO|OUTRAS-ORIENTACOES-CONCLUIDAS':
            self.ultimoPublicou = True
            tmpTipo = ArtigosPublicados.tag
            tmpTag = "DE-" + ArtigosPublicados.tag
            tmpTagAno = ""
        else:
            if self.utilizarProducoesTecnicas:
                if ArtigosPublicados.tag == 'CULTIVAR-REGISTRADA':
                    self.ultimoPublicou = True
                    tmpTipo = "CULTIVAR-REGISTRADA"
                    tmpTag = "DA-CULTIVAR"
                    tmpTagAno = "-SOLICITACAO"
                elif ArtigosPublicados.tag == 'SOFTWARE':
                    self.ultimoPublicou = True
                    tmpTipo = "SOFTWARE"
                    tmpTag = "DO-SOFTWARE"
                    tmpTagAno = ""
                elif ArtigosPublicados.tag == 'PATENTE':
                    self.ultimoPublicou = True
                    tmpTipo = "PATENTE"
                    tmpTag = "DA-PATENTE"
                    tmpTagAno = "-DESENVOLVIMENTO"
                elif ArtigosPublicados.tag == 'CULTIVAR-PROTEGIDA':
                    self.ultimoPublicou = True
                    tmpTipo = "CULTIVAR-PROTEGIDA"
                    tmpTag = "DA-CULTIVAR"
                    tmpTagAno = "-SOLICITACAO"
                elif ArtigosPublicados.tag == 'DESENHO-INDUSTRIAL':
                    self.ultimoPublicou = True
                    tmpTipo = "DESENHO-INDUSTRIAL"
                    tmpTag = "DO-DESENHO-INDUSTRIAL"
                    tmpTagAno = "-DESENVOLVIMENTO"
                elif ArtigosPublicados.tag == 'MARCA':
                    self.ultimoPublicou = True
                    tmpTipo = "MARCA"
                    tmpTag = "DA-MARCA"
                    tmpTagAno = "-DESENVOLVIMENTO"
                elif ArtigosPublicados.tag == 'TOPOGRAFIA-DE-CIRCUITO-INTEGRADO':
                    self.ultimoPublicou = True
                    tmpTipo = "TOPOGRAFIA-DE-CIRCUITO-INTEGRADO"
                    tmpTag = "DA-TOPOGRAFIA-DE-CIRCUITO-INTEGRADO"
                    tmpTagAno = "-DESENVOLVIMENTO"
                elif ArtigosPublicados.tag == 'PRODUTO-TECNOLOGICO':
                    self.ultimoPublicou = True
                    tmpTipo = "PRODUTO-TECNOLOGICO"
                    tmpTag = "DO-PRODUTO-TECNOLOGICO"
                    tmpTagAno = ""
                elif ArtigosPublicados.tag == 'PROCESSOS-OU-TECNICAS':
                    self.ultimoPublicou = True
                    tmpTipo = "PROCESSOS-OU-TECNICAS"
                    tmpTag = "DO-PROCESSOS-OU-TECNICAS"
                    tmpTagAno = ""
                elif ArtigosPublicados.tag == 'TRABALHO-TECNICO':
                    self.ultimoPublicou = True
                    tmpTipo = "TRABALHO-TECNICO"
                    tmpTag = "DO-TRABALHO-TECNICO"
                    tmpTagAno = ""

        if (tmpTag != ""):
            self.extrai_coautores(ArtigosPublicados, ListaDois, QuantidadeComDoi, QuantidadeSemDoi, Coautores,
                                  tmpTipo, tmpTag, tmpTagAno)


    # Funcao que caracteriza cada pessoa
    # Retorna uma lista somente com identificador do Latttes, se nao eh doutor
    # Se doutor, retorna uma lista com dois conjuntos:
    # um com seu nome em citacoes e outro com seus coautores
    def extrai(self, path, lattes):
        #tree = ET.parse(self.obter_arquivo(os.path.join(path, lattes)))
        root = ET.fromstring(self.obter_arquivo(os.path.join(path, lattes)).encode('ISO-8859-1') ) #tree.getroot()
        DadosGerais = root[0]
        Identificador = lattes.replace('.xml', '')

        # Obtem o nome em citacoes
        Autor = Set()
        if not (DadosGerais.text is None) and  ('Erro' in DadosGerais.text):
            self.exibir_andamento("XML invalido: "+ Identificador, logging.INFO)
        else:
            autores = [DadosGerais.attrib['NOME-EM-CITACOES-BIBLIOGRAFICAS']]
            autores = self.nomes(autores)

            # Adiciona o identificador no lattes, como alternativa aos nomes do autor
            Autor.add(Identificador)

            for aux in autores:
                temp_autor = self.limpa_autor(aux)
                if not (temp_autor in Autor) and temp_autor != '':
                    Autor.add(temp_autor)

            # Coleta os nomes dos coautores em artigos publicados
            Coautores = []
            rangeAnos = 0
            if not (self.agruparAnos):
                rangeAnos = self.AnoFinal - self.AnoInicial

            for ano in range(rangeAnos+1): # Cria sets separados de coautores para cada conjunto de anos avaliados (quando necessario)
                Coautores.append(Set())

            ListaDois = Set()
            QuantidadeSemDoi = 0
            QuantidadeComDoi = 0
            self.ultimoPublicou = False

            for indice in range(len(root)):
                raiz = root[indice]
                if (raiz.tag in "|PRODUCAO-BIBLIOGRAFICA|PRODUCAO-TECNICA|OUTRA-PRODUCAO|"):
                    for index in range(len(raiz)):
                        ArtigosPublicados = raiz[index]
                        self.extrai_publicacao(ArtigosPublicados, ListaDois, QuantidadeComDoi, QuantidadeSemDoi,
                                          Coautores)

            if not self.ultimoPublicou:
                self.exibir_andamento("Sem pubs: "+ str(self.semPubs), logging.DEBUG)
                self.semPubs = self.semPubs + 1
                return [Identificador]

            # Remove o proprio autor da lista dos coautores
            for c in range(len(Coautores)):
                Coautores[c] = Coautores[c] - Autor
                if len(Coautores[c])<=0:
                    self.exibir_andamento("sem pubs: " + str(self.semPubs), logging.DEBUG)
                    self.semPubs = self.semPubs + 1
                    return [Identificador]

            if not (self.agruparAnos):
                for i in range(1 + self.AnoFinal - self.AnoInicial):
                    self.Lattes[i].append([Identificador, Autor, Coautores[i], ListaDois, QuantidadeComDoi, QuantidadeSemDoi])
            else:
                self.Lattes[0].append([Identificador, Autor, Coautores[0], ListaDois, QuantidadeComDoi, QuantidadeSemDoi])

    # Retorna o indice do ano escolhido dentro da faixa de anos fornecida
    def get_year_index(self, initial, chosen):
        if self.agruparAnos:
            return 0
        else:
            return chosen-initial

    # Metodo para salvar pdfs
    def download_file(self, url, local_filename):
        cabecalho = {
            'user-agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36'}

        r = requests.get(url, cabecalho, stream=True, timeout=40)
        self.iniciar_gravar_arquivo(local_filename, TipoContainer.pdfs)
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                self.continuar_gravar_arquivo(chunk)
        self.encerrar_gravar_arquivo()

        return local_filename

    # Metodo que busca as informacoes sobre o Artigo, a partir de seu DOI no doi.org
    def buscar_datas_doi(self, doi="", urlCompleta=""):
        try:
            # sem este cabeďçalho que simula a requisiďç?o feita pelo Chrome, alguns servidores rejeitam a requisiďç?o
            cabecalho = {
                'user-agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36'}

            if doi:
                self.exibir_andamento("http://dx.doi.org/" + doi)
                # Verifica se utilizarĂĄ proxy
                if self.usarProxy:
                    pagina = requests.get("http://dx.doi.org/" + doi, headers=cabecalho, timeout=40,
                                          proxies=self.proxyRequisicao)
                else:
                    pagina = requests.get("http://dx.doi.org/" + doi, headers=cabecalho, timeout=40)

            else:
                if self.usarProxy:
                    pagina = requests.get(urlCompleta, headers=cabecalho, timeout=40, proxies=self.proxyRequisicao)
                else:
                    pagina = requests.get(urlCompleta, headers=cabecalho, timeout=40)

            pagina.encoding = 'ISO-8859-1'
            retorno = pagina.content
            return retorno
        except ConnectionError as e:
            self.exibir_andamento('Erro: ')
            self.exibir_andamento(e)
            time.sleep(20)
            return ""
        except:
            self.exibir_andamento('Erro')
            self.exibir_andamento(sys.exc_info()[0])
            time.sleep(20)
            return ""

    # Retorna a indicacao de parceria entre dois nos da rede
    def GetPosition(self, cv, i, j):
        pessoa_1 = cv[i]
        pessoa_2 = cv[j]

        if self.validarMatrizDoisSentidos:
            if len(pessoa_1[1].intersection(pessoa_2[2])) != 0 and len(pessoa_2[1].intersection(pessoa_1[2])) != 0:
                return 1
            else:
                return 0
        else:
            if len(pessoa_1[1].intersection(pessoa_2[2])) != 0 or len(pessoa_2[1].intersection(pessoa_1[2])) != 0:
                return 1
            else:
                return 0

    def gerar_nome_periodo(self, somaAno = 0):
        # Formatando o titulo do periodo
        if self.agruparAnos:
            return str(self.AnoInicial) + '-' + str(self.AnoFinal)
        else:
            return str(self.AnoInicial + somaAno)

    # Inicializa o processo de geracao da matriz
    def processar_lattes(self, container = TipoContainer.idsets):
        # blob_service = storage_account.create_blob_service() #para habilitar autocomplete
        self.inicializa_lattes()

        arquivoIDs = self.obter_arquivo(self.nomeArquivoIDs, container) #open(nomeArquivoIDs, 'r')
        if self.tipoArmazenamento != TipoArmazenamento.local:
            self.gravar_arquivo_se_nao_existe(os.path.join(self.obter_saida_formatada(), "ids_utilizados.txt"),
                                          arquivoIDs, TipoContainer.results)

        #ListaLattes = os.listdir(nomePastaCurriculos)
        self.exibir_andamento('Buscando tokens ' + self.nomeArquivoIDs, logging.WARN)

        totalProcessados = 0

        for linha in string.split(arquivoIDs, '\n'):
            try:
                if self.converterToken:
                    nomeArqToken = self.nomePastaTokens + linha.strip() + ".txt"
                    if self.verifica_arquivo_existe(nomeArqToken, TipoContainer.lattesextraido):
                        arqToken = self.obter_arquivo(nomeArqToken, TipoContainer.lattesextraido) #open(nomeArqToken, "r")
                        pessoa = str(arqToken.strip()) + '.xml'
                    else:
                        pessoa = ".xml"
                else:
                    pessoa = str(linha.strip()) + '.xml'

                if self.verifica_arquivo_existe(os.path.join(self.nomePastaCurriculos, pessoa), TipoContainer.lattesextraido):
                    self.extrai(self.nomePastaCurriculos, pessoa)
                    totalProcessados += 1

                    if totalProcessados>=(self.frequenciaStatus) and (totalProcessados %  self.frequenciaStatus) == 0:
                        self.exibir_andamento("Sem publicaçoes no periodo: " + str(self.semPubs), logging.INFO)
                        self.exibir_andamento(self.nomeArquivoIDs + " Processados: " + str(totalProcessados), logging.WARN)
                    else:
                        self.exibir_andamento(self.nomeArquivoIDs + "Processados: " + str(totalProcessados), logging.DEBUG)
                else:
                    self.exibir_andamento("XML do curriculo nao encontrado: " + str(pessoa), logging.INFO)

            except Exception as e:
                if e.message == 'NOME-EM-CITACOES-BIBLIOGRAFICAS':
                    self.exibir_andamento("'Arquivo XML invalido: ', '{0}' - {1}".format(linha.strip('\r\n'), str(e)),
                                          logging.INFO)
                else:
                    self.exibir_andamento("'Exception: ', '{0}' - {1}".format(linha.strip('\r\n'), str(e)),
                                          logging.FATAL)

        for ano in range(self.rangeAnos+1):
            cvs = self.Lattes[ano]
            n = len(cvs)

            self.exibir_andamento('Gerando arquivos do periodo: ' + self.gerar_nome_periodo())

            if self.gerarDOI:
                self.exibir_andamento('Inicio geracao resumo DOI')
                self.iniciar_gravar_arquivo(self.nomeArquivoResumoDOI.format(self.nomeArea, self.gerar_nome_periodo()),
                                             TipoContainer.results)

                for i in range(0, n):
                    cv = cvs[i]
                    self.continuar_gravar_arquivo('[' + str(cv[0]) + ']')
                    self.continuar_gravar_arquivo('; ')
                    self.continuar_gravar_arquivo(str(cv[1]))
                    self.continuar_gravar_arquivo('; ')
                    self.continuar_gravar_arquivo(str(cv[3]))
                    self.continuar_gravar_arquivo('; ')
                    self.continuar_gravar_arquivo(str(cv[4]))
                    self.continuar_gravar_arquivo('; ')
                    self.continuar_gravar_arquivo(str(cv[5]))
                    self.continuar_gravar_arquivo('\n')

                self.exibir_andamento('Fechando resumo DOI')
                self.encerrar_gravar_arquivo()

            # Escreve em csv - pares de conexoes
            # TODO: escrever arquivo com nos sem pares
            if self.gerarPares:
                self.nomeArquivoPares = self.nomePadraoArquivo.format(self.obter_saida_formatada(), self.AnoInicial,
                                                                      self.AnoFinal)
                self.exibir_andamento('Inicio geracao arquivo pares: ' + self.nomeArquivoPares)
                self.exibir_andamento(self.nomeArquivoPares, logging.DEBUG)
                self.iniciar_gravar_arquivo(self.nomeArquivoPares, TipoContainer.results)

                # Escreve conteudo do csv
                for i in range(n):
                    for j in range(i, n):
                        if (self.GetPosition(cvs, i, j)) == 1:
                            self.continuar_gravar_arquivo(cvs[i][0])
                            self.continuar_gravar_arquivo(';')
                            self.continuar_gravar_arquivo(cvs[j][0])
                            self.continuar_gravar_arquivo('\n')

                    self.exibir_andamento('Andamento pares: ' + str(i+1) + ' de ' + str(n), logging.DEBUG)
                self.exibir_andamento('Fim geracao arquivo pares')
                self.encerrar_gravar_arquivo()

            # Salva no formato para o Octave
            if self.gerarOctave:
                self.exibir_andamento('Inicio geracao arquivo Octave')
                self.exibir_andamento(self.nomeArquivoOctave.format(self.nomeArea, self.gerar_nome_periodo()))
                self.iniciar_gravar_arquivo(self.nomeArquivoOctave.format(self.nomeArea, self.gerar_nome_periodo()), TipoContainer.results)
                self.continuar_gravar_arquivo('# name: A\n')
                self.continuar_gravar_arquivo('# type: matrix\n')
                self.continuar_gravar_arquivo('# rows: ' + str(n) + '\n')
                self.continuar_gravar_arquivo('# columns: ' + str(n) + '\n')
                for i in range(n):
                    for j in range(n):
                        self.continuar_gravar_arquivo(str(self.GetPosition(cvs, i, j)) + ' ')

                    self.continuar_gravar_arquivo('\n')
                    self.exibir_andamento('Andamento octave: ' + str(i+1) + ' de ' + str(n), logging.DEBUG)
                self.exibir_andamento('Final geracao arquivo Octave')
                self.encerrar_gravar_arquivo()

            # Escreve em csv - cabeďçalho - Formato Gephi
            # TODO: mudar para nos mais pares
            if self.gerarGephi:
                self.exibir_andamento('Inicio geracao arquivo Gephi')
                self.iniciar_gravar_arquivo(self.nomeArquivoGephi.format(self.nomeArea, self.gerar_nome_periodo()), TipoContainer.results)

                # Escreve uma cabecalho com os dados da primeira coluna de cada linha
                for i in range(n):
                    self.continuar_gravar_arquivo(';' + cvs[i][0])
                self.continuar_gravar_arquivo('\n')
                # Escreve conteudo do csv
                for i in range(n):
                    self.continuar_gravar_arquivo(cvs[i][0])
                    for j in range(n):
                        self.continuar_gravar_arquivo(';' + str(self.GetPosition(cvs, i, j)))

                    self.continuar_gravar_arquivo('\n')
                    self.exibir_andamento('Andamento Gephi: ' + str(i+1) + ' de ' + str(n), logging.DEBUG)
                self.exibir_andamento('Fim geracao arquivo Gephi')
                self.encerrar_gravar_arquivo()

            # Escreve em csv - cabecalho - Formato Pandas outdated
            if self.gerarPandas:
                self.exibir_andamento('Inicio geracao arquivo Pandas')
                self.iniciar_gravar_arquivo(self.nomeArquivoPandas.format(self.nomeArea, self.gerar_nome_periodo()),
                                            TipoContainer.results)

                # Escreve conteudo do csv
                for i in range(n):
                    for j in range(n):
                        # Escrever o par (vertice) apenas uma vez
                        if (cvs[i][0]<cvs[j][0] and self.GetPosition(cvs, i, j)==1):
                            self.continuar_gravar_arquivo(cvs[i][0])
                            self.continuar_gravar_arquivo(';' + cvs[j][0])
                            self.continuar_gravar_arquivo('\n')
                self.exibir_andamento('Fim geracao arquivo Pandas')
                self.encerrar_gravar_arquivo()

        if not self.obter_saida_formatada() in self.listaSaidas:
            self.listaSaidas.append(self.obter_saida_formatada())
        self.exibir_andamento('Geracao de pares finalizada: ' + self.obter_saida_formatada(), logging.INFO)

    # Local only
    def extract_mag(self, diretorioRaiz, cod_pub, anoInicial, anoFinal, trimestre = 0, mes = 0, minimoAutores = 0):
        diretorioBase = diretorioRaiz + cod_pub + "\\"
        # Filtro por ano das publicaçoes
        self.AnoInicial = anoInicial
        self.AnoFinal = anoFinal
        self.nomeLogErros = os.path.join(diretorioBase, 'erros.csv')
        if trimestre ==0 and mes ==0:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        'stats-' + str(self.AnoInicial) + '-' + str(self.AnoFinal) + '.csv')
        elif trimestre <> 0:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-Q" + str(trimestre) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        "stats-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-Q" + str(trimestre) + ".csv")
        else:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-M" + str(mes) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        "stats-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-M" + str(mes) + ".csv")

        inicio = time.time()
        executarArea = True

        try:
            if not os.path.exists(diretorioBase):
                os.makedirs(diretorioBase)

            if not (os.path.isfile(self.nomeArquivoPares)):
                try:                    
                    cnx = mysql.connector.connect(user='pythonuser', password='pythonuser', host='127.0.0.1',
                                                  database='pesquisa', raise_on_warnings=False)
                    cursor = cnx.cursor()
    
                    self.exibir_andamento('Limpando tabelas', logging.DEBUG)
                    # Limpa tabela temporaria de relacionamento de artigo com autor
                    #query = (" DROP TEMPORARY TABLE IF EXISTS paper_author_affiliation_temp3")
                    #cursor.execute(query)
                    tempguid = str(uuid.uuid1()).replace('-', '')
    
                    query = ("CREATE TABLE `paa" + tempguid + "` ("
                                "`paper_id` varchar(12) NOT NULL, "
                                "`author_id` varchar(12) DEFAULT NULL, "
                                "`author_order` int(11) DEFAULT NULL,"
                                " KEY `author_ix` (`author_id`) ,"
                                " KEY `paper_ix` (`paper_id`) "
                                ") ENGINE=MEMORY DEFAULT CHARSET=utf8;")
                    cursor.execute(query)
    
                    # Limpa tabela temporaria de artigos (filtrados)
                    #query = (" DROP TEMPORARY TABLE IF EXISTS papers_temp3")
                    #cursor.execute(query)
    
                    query = (" CREATE TABLE `p" + tempguid + "` ("
                             " `paper_id` varchar(12) NOT NULL,"
                             " `publish_year` varchar(12) DEFAULT NULL,"
                             " `publish_date` varchar(15) DEFAULT NULL,"
                             " `jornal_id` varchar(12) DEFAULT NULL,"
                             " PRIMARY KEY (`paper_id`), "
                             " UNIQUE KEY `idx_papers_temp_paper_id` (`paper_id`),"
                             " KEY `jornal_ix` (`jornal_id`) "
                             " ) ENGINE=MEMORY DEFAULT CHARSET=utf8;")
                    cursor.execute(query)
    
                    self.exibir_andamento('Populando artigos ' + cod_pub, logging.DEBUG)
                    if (trimestre == 0 and mes == 0):
                        query = (" insert into p" + tempguid +
                                 " select * from papers_temp "
                                 " where (jornal_id = '" + cod_pub + "') ")
                    elif (trimestre <> 0):
                        query = (" insert into p" + tempguid +
                                 " select * from papers_temp "
                                 " where (jornal_id = '" + cod_pub + "')  and length(publish_date) >6 and "
                                 "( (cast(substring(publish_date, 6, 2) as signed)-1) DIV 3) = " + str(trimestre-1) )
                    else: # mês
                        query = (" insert into p" + tempguid +
                                 " select * from papers_temp "
                                 " where (jornal_id = '" + cod_pub + "')  and length(publish_date) >6 and "
                                 "(cast(substring(publish_date, 6, 2) as signed) = " + str(mes) + " )")
                    #print query
                    cursor.execute(query)
    
                    self.exibir_andamento( 'Populando autores ' + cod_pub, logging.DEBUG)
                    query = (" insert into paa" + tempguid +
                             " select paper_id, author_id, author_order from paper_author_affiliation_temp "
                             " where paper_id in (select paper_id from p" + tempguid + ")")
                    cursor.execute(query)
                    # cursor.execute(query, (hire_start, hire_end))


                    # Se for pra validar a quantidade minima de autores
                    if minimoAutores>0:
                        query = (" select count(distinct author_id) from paa" + tempguid)
                        try:
                            cursor.execute(query)
                            quantidade = int(cursor.fetchone()[0])
                        except Exception as e:
                            self.exibir_andamento("Erro: " + str(e), logging.FATAL)

                        if (quantidade < minimoAutores):
                            executarArea = False
                            self.exibir_andamento("Minimo nao atingindo: " + str(quantidade), logging.WARNING)

                    self.exibir_andamento( 'Buscando autores ' + cod_pub, logging.DEBUG)

                    if executarArea:
                        # Query de retorno dos pares
                        query = (" SELECT distinct t1.author_id, t2.author_id "
                                 " FROM ("
                                 " select author_id, p.paper_id from "
                                 " pesquisa.p" + tempguid + " p inner join pesquisa.paa" + tempguid + " paa on p.paper_id=paa.paper_id "
                                 " where jornal_id='" + cod_pub + "') t1 inner join ( "
                                                                  " select author_id, p.paper_id from "
                                                                  " pesquisa.p" + tempguid + " p inner join pesquisa.paa" + tempguid + " paa on p.paper_id=paa.paper_id "
                                                                  " where jornal_id='" + cod_pub + "') t2 on t1.paper_id=t2.paper_id and t1.author_id<t2.author_id ")
                        cursor.execute(query)

                        # Criando arquivo
                        self.exibir_andamento( 'Criando arquivo ' + cod_pub, logging.DEBUG)
                        saida = open(self.nomeArquivoPares, 'w')
                        for (author1, author2) in cursor:
                            saida.write("{};{}\n".format(author1, author2))

                        saida.close()
                        # Arquivo fechado
                        self.exibir_andamento( 'Arquivo fechado ' + cod_pub, logging.DEBUG)
                    #else:
                        #os.remove(diretorioBase)
    
                    query = (" drop TABLE IF EXISTS p" + tempguid)
                    cursor.execute(query)
    
                    query = (" drop TABLE IF EXISTS paa" + tempguid)
                    cursor.execute(query)
                finally:
                    if cnx:
                        query = (" drop TABLE IF EXISTS p" + tempguid)
                        cursor.execute(query)
        
                        query = (" drop TABLE IF EXISTS paa" + tempguid)
                        cursor.execute(query)
        
                        cursor.close()
                        cnx.close()
            else:
                self.exibir_andamento( 'Arquivo ja gerado ' + self.nomeArquivoPares, logging.DEBUG)

            if not executarArea:
                os.removedirs(diretorioBase)

            if False: #executarArea:  apenas gerar os pares, por enquanto
                total_nos = 0

                statinfo = os.stat(self.nomeArquivoPares)
                self.exibir_andamento('Pair file Size: ' + str(statinfo.st_size), logging.DEBUG)
                self.exibir_andamento('Stats file: ' + arquivoStats, logging.DEBUG)
                if statinfo.st_size > 1 and not (os.path.isfile(arquivoStats)):
                    passado = pd.read_csv(self.nomeArquivoPares, names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';',
                                          header=None)
                    amplo = nx.from_pandas_dataframe(passado, 'a', 'b')
                    G = nx.Graph(amplo)
                    if G.number_of_edges() > 1:
                        self.exibir_andamento('Imprimir stats comecando ' + cod_pub, logging.DEBUG)
                        try:
                            self.imprimir_stats(G, total_nos, arquivoStats)
                        except Exception as e:
                            self.exibir_andamento('Erro ao gerar stats '+ e.message + self.nomeArquivoPares, logging.ERROR)
                    else:
                        self.exibir_andamento('Rede com poucos nos', logging.WARNING)
            else:
                self.exibir_andamento('Arquivo vazio ou ja criado '+self.nomeArquivoPares, logging.WARNING)
        except Exception as e:
            self.exibir_andamento(str(e), logging.FATAL)
        
        self.exibir_andamento(inicio, logging.DEBUG)
        # fim
        self.exibir_andamento('Fim ' + cod_pub, logging.DEBUG)
        self.exibir_andamento(time.time() - inicio, logging.DEBUG)

    def extract_mag_old(self, diretorioRaiz, cod_pub, anoInicial, anoFinal, trimestre=0, mes=0, minimoAutores=0):
        diretorioBase = diretorioRaiz + cod_pub + "\\"
        # Filtro por ano das publicaçoes
        self.AnoInicial = anoInicial
        self.AnoFinal = anoFinal
        self.nomeLogErros = os.path.join(diretorioBase, 'erros.csv')
        if trimestre == 0 and mes == 0:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        'stats-' + str(self.AnoInicial) + '-' + str(self.AnoFinal) + '.csv')
        elif trimestre <> 0:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-Q" + str(
                                                     trimestre) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        "stats-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-Q" + str(
                                            trimestre) + ".csv")
        else:
            self.nomeArquivoPares = os.path.join(diretorioBase,
                                                 "pair-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-M" + str(
                                                     mes) + ".csv")
            arquivoStats = os.path.join(diretorioBase,
                                        "stats-" + str(self.AnoInicial) + "-" + str(self.AnoFinal) + "-M" + str(
                                            mes) + ".csv")

        inicio = time.time()
        executarArea = True

        try:
            if not os.path.exists(diretorioBase):
                os.makedirs(diretorioBase)

            if not (os.path.isfile(self.nomeArquivoPares)):
                try:
                    cnx = mysql.connector.connect(user='pythonuser', password='pythonuser', host='127.0.0.1',
                                                  database='pesquisa', raise_on_warnings=False)
                    cursor = cnx.cursor()

                    self.exibir_andamento('Limpando tabelas', logging.DEBUG)
                    # Limpa tabela temporaria de relacionamento de artigo com autor
                    # query = (" DROP TEMPORARY TABLE IF EXISTS paper_author_affiliation_temp3")
                    # cursor.execute(query)
                    tempguid = str(uuid.uuid1()).replace('-', '')

                    query = ("CREATE TABLE `paa" + tempguid + "` ("
                                                              "`paper_id` varchar(12) NOT NULL, "
                                                              "`author_id` varchar(12) DEFAULT NULL, "
                                                              " `affiliation_id` varchar(12) DEFAULT NULL,"
                                                              "`original_affiliation` varchar(45) DEFAULT NULL,"
                                                              "`author_order` int(11) DEFAULT NULL"
                                                              ") ENGINE=MEMORY DEFAULT CHARSET=utf8;")
                    cursor.execute(query)

                    # Limpa tabela temporaria de artigos (filtrados)
                    # query = (" DROP TEMPORARY TABLE IF EXISTS papers_temp3")
                    # cursor.execute(query)

                    query = (" CREATE TABLE `p" + tempguid + "` ("
                                                             " `paper_id` varchar(12) NOT NULL,"
                                                             " `original_title` varchar(50) CHARACTER SET utf16 DEFAULT NULL,"
                                                             " `normalized_title` varchar(200) DEFAULT NULL,"
                                                             " `publish_year` varchar(12) DEFAULT NULL,"
                                                             " `publish_date` varchar(15) DEFAULT NULL,"
                                                             " `doi` varchar(50) DEFAULT NULL,"
                                                             " `original_venue` varchar(150) CHARACTER SET utf16 DEFAULT NULL,"
                                                             " `normalized_venue` varchar(150) DEFAULT NULL,"
                                                             " `jornal_id` varchar(12) DEFAULT NULL,"
                                                             " `conference_series_id` varchar(12) DEFAULT NULL,"
                                                             " `page_rank` decimal(18,0) DEFAULT NULL,"
                                                             " UNIQUE KEY `idx_papers_temp_paper_id` (`paper_id`)"
                                                             " ) ENGINE=MEMORY DEFAULT CHARSET=utf8;")
                    cursor.execute(query)

                    self.exibir_andamento('Populando artigos ' + cod_pub, logging.DEBUG)
                    if (trimestre == 0 and mes == 0):
                        query = (" insert into p" + tempguid +
                                 " select * from papers "
                                 " where publish_year<>'' and not publish_year is null and instr(publish_year, '\\\\') = 0 and "
                                 " instr(publish_year, '/') = 0 and (cast(publish_year as signed) BETWEEN  " + str(
                            self.AnoInicial) + " and " + str(self.AnoFinal) + " ) AND (jornal_id = '" +
                                 cod_pub + "') ")
                    elif (trimestre <> 0): # need fix divisao por 4
                        query = (" insert into p" + tempguid +
                                 " select * from papers "
                                 " where publish_year<>'' and not publish_year is null and instr(publish_year, '\\\\') = 0 and "
                                 " instr(publish_year, '/') = 0 and (cast(publish_year as signed) BETWEEN  " + str(
                            self.AnoInicial) + " and " + str(self.AnoFinal) + " ) AND (jornal_id = '" +
                                 cod_pub + "')  and length(publish_date) >6 and ( (cast(substring(publish_date, 6, 2) as signed)-1) DIV 3) = "
                                 + str(trimestre - 1))
                    else:  # mês
                        query = (" insert into p" + tempguid +
                                 " select * from papers "
                                 " where publish_year<>'' and not publish_year is null and instr(publish_year, '\\\\') = 0 and "
                                 " instr(publish_year, '/') = 0 and (cast(publish_year as signed) BETWEEN  " + str(
                            self.AnoInicial) + " and " + str(self.AnoFinal) + " ) AND (jornal_id = '" +
                                 cod_pub + "')  and length(publish_date) >6 and (cast(substring(publish_date, 6, 2) as signed) = "
                                 + str(mes) + " )")
                    # print query
                    cursor.execute(query)

                    self.exibir_andamento('Populando autores ' + cod_pub, logging.DEBUG)
                    query = (" insert into paa" + tempguid +
                             " select paper_id, author_id, affiliation_id, original_affiliation, author_order from paper_author_affiliation "
                             " where paper_id in (select paper_id from p" + tempguid + ")")
                    cursor.execute(query)
                    # cursor.execute(query, (hire_start, hire_end))


                    # Se for pra validar a quantidade minima de autores
                    if minimoAutores > 0:
                        query = (" select count(distinct author_id) from paa" + tempguid)
                        try:
                            cursor.execute(query)
                            quantidade = int(cursor.fetchone()[0])
                        except Exception as e:
                            self.exibir_andamento("Erro: " + str(e), logging.FATAL)

                        if (quantidade < minimoAutores):
                            executarArea = False
                            self.exibir_andamento("Minimo nao atingindo: " + str(quantidade), logging.WARNING)

                    self.exibir_andamento('Buscando autores ' + cod_pub, logging.DEBUG)

                    if executarArea:
                        # Query de retorno dos pares
                        query = (" SELECT distinct t1.author_id, t2.author_id "
                                 " FROM ("
                                 " select author_id, p.paper_id from "
                                 " pesquisa.p" + tempguid + " p inner join pesquisa.paa" + tempguid + " paa on p.paper_id=paa.paper_id "
                                                                                                      " where jornal_id='" + cod_pub + "') t1 inner join ( "
                                                                                                                                       " select author_id, p.paper_id from "
                                                                                                                                       " pesquisa.p" + tempguid + " p inner join pesquisa.paa" + tempguid + " paa on p.paper_id=paa.paper_id "
                                                                                                                                                                                                            " where jornal_id='" + cod_pub + "') t2 on t1.paper_id=t2.paper_id and t1.author_id<t2.author_id ")
                        cursor.execute(query)

                        # Criando arquivo
                        self.exibir_andamento('Criando arquivo ' + cod_pub, logging.DEBUG)
                        saida = open(self.nomeArquivoPares, 'w')
                        for (author1, author2) in cursor:
                            saida.write("{};{}\n".format(author1, author2))

                        saida.close()
                        # Arquivo fechado
                        self.exibir_andamento('Arquivo fechado ' + cod_pub, logging.DEBUG)
                        # else:
                        # os.remove(diretorioBase)

                    query = (" drop TABLE IF EXISTS p" + tempguid)
                    cursor.execute(query)

                    query = (" drop TABLE IF EXISTS paa" + tempguid)
                    cursor.execute(query)
                finally:
                    if cnx:
                        query = (" drop TABLE IF EXISTS p" + tempguid)
                        cursor.execute(query)

                        query = (" drop TABLE IF EXISTS paa" + tempguid)
                        cursor.execute(query)

                        cursor.close()
                        cnx.close()
            else:
                self.exibir_andamento('Arquivo ja gerado ' + self.nomeArquivoPares, logging.DEBUG)

            if executarArea:
                total_nos = 0

                statinfo = os.stat(self.nomeArquivoPares)
                self.exibir_andamento('Pair file Size: ' + str(statinfo.st_size), logging.DEBUG)
                self.exibir_andamento('Stats file: ' + arquivoStats, logging.DEBUG)
                if statinfo.st_size > 1 and not (os.path.isfile(arquivoStats)):
                    passado = pd.read_csv(self.nomeArquivoPares, names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';',
                                          header=None)
                    amplo = nx.from_pandas_dataframe(passado, 'a', 'b')
                    G = nx.Graph(amplo)
                    if G.number_of_edges() > 1:
                        self.exibir_andamento('Imprimir stats comecando ' + cod_pub, logging.DEBUG)
                        try:
                            self.imprimir_stats(G, total_nos, arquivoStats)
                        except Exception as e:
                            self.exibir_andamento('Erro ao gerar stats ' + e.message + self.nomeArquivoPares,
                                                  logging.ERROR)
                    else:
                        self.exibir_andamento('Rede com poucos nos', logging.WARNING)
            else:
                self.exibir_andamento('Arquivo vazio ou ja criado ' + self.nomeArquivoPares, logging.WARNING)
        except Exception as e:
            self.exibir_andamento(str(e), logging.FATAL)

        self.exibir_andamento(inicio, logging.DEBUG)
        # fim
        self.exibir_andamento('Fim ' + cod_pub, logging.DEBUG)
        self.exibir_andamento(time.time() - inicio, logging.DEBUG)

    # recebe o conteudo de um arquivo de pares e retorna o conteudo de um arquivo de ids
    def montar_lista_nos_de_lista_pares(self, conteudo_arq):
        passado = pd.read_csv(StringIO(conteudo_arq), names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';', header=None)

        # Carregamento da rede com os nos do passado
        redePassado = nx.from_pandas_dataframe(passado, 'a', 'b')

        G = nx.Graph(redePassado)

        return '\n\r'.join(G.nodes())

    # Fachada para chamada da extraçao MAG
    def execucao_mag(self, diretorioRaiz, lista, titulo, anoInicio, anoFim, trimestre=0, mes=0, old=True):
        lista2 = list(lista)
        atual = 1
        total = len(lista2)
        for cod_pub in lista2:
            self.exibir_andamento(titulo + str(atual) + " de " + str(total), logging.INFO)
            atual = atual + 1
            if old:
                self.extract_mag_old(diretorioRaiz, cod_pub, anoInicio, anoFim, trimestre, mes)
            else:
                self.extract_mag(diretorioRaiz, cod_pub, anoInicio, anoFim, trimestre, mes)

    # Envia arquivo de log local para a pasta dos resultados do experimento
    def enviar_log(self):
        f = open(self.inicio_experimento + '.log', 'r')
        self.gravar_arquivo(self.nomePastaSaida.format("logs") + datetime.datetime.now().strftime("%Y%m%d-%H%M")+".log",
                            '\n'.join(f.readlines()), TipoContainer.results)
        f.close()
        self.exibir_andamento('Log enviado!')

    def listar_periodicos_mag(self):
        try:
            cnx = mysql.connector.connect(user='pythonuser', password='pythonuser', host='127.0.0.1',
                                          database='pesquisa', raise_on_warnings=False)
            cursor = cnx.cursor()

            # Periodicos com mais de 450 publicaçoes em 5 anos. (Publicaçao bimestral com 15 artigos por ediçao)
            query = (" select jornal_id, journal_name from papers_temp p inner join journals j on p.jornal_id=j.journal_id "
                    " where not (jornal_id is null)  and  CAST(publish_year AS UNSIGNED) between 2012 and 2014 and jornal_id <> '' "
                    " group by jornal_id, journal_name "
                    " having count(*)>144 " # 12 artigos por trimestre
                    " order by count(*) asc")
            cursor.execute(query)

            retorno = list(cursor.fetchall())
            return retorno
        finally:
            if cnx:
                cursor.close()
                cnx.close()
        return