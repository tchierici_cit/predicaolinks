# -*- coding: utf8 -*-
import logging

from azure.storage.blob import BlockBlobService
import pandas as pd
import os
import networkx as nx
from networkLib import *
import operator

basedir = "f:\\pesquisa\\mag\\"
nl = NetworkLib(TipoArmazenamento.local, experimento="integrar" )
i = 0

saidaordenada = {}

for root, dirs, files in os.walk(basedir):
    for file in files:
        nomeArquivoPares = os.path.join(root, file)
        arquivoStats= os.path.join(root, 'stats-2012-2014.csv')
        statinfo = os.stat(nomeArquivoPares)
        if "pair" in nomeArquivoPares:            
            saidaordenada[i] = {'size': statinfo.st_size, 'file': nomeArquivoPares, 'stats': arquivoStats}
            i = i + 1

novalista = sorted(saidaordenada.items(), key=lambda x: x[1]['size'])
i = 0

for s in novalista:
    i = i + 1
    nl.exibir_andamento('Processando: ' + str(i))
    nomeArquivoPares = s[1]['file']
    arquivoStats = s[1]['stats']

    if not (os.path.isfile(arquivoStats)):
        nl.exibir_andamento("Nao processado - Tamanho (kb): " + str(s[1]['size'] / 1024) + " - " + nomeArquivoPares,
                            logging.DEBUG)
        passado = pd.read_csv(nomeArquivoPares, names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';',
                              header=None)
        amplo = nx.from_pandas_dataframe(passado, 'a', 'b')
        G = nx.Graph(amplo)
        if G.number_of_edges() > 1:
            try:
                nl.imprimir_stats(G, G.number_of_nodes(), arquivoStats)
            except Exception as e:
                nl.exibir_andamento('Erro ao gerar stats ' + e.message + nomeArquivoPares, logging.ERROR)
        else:
            nl.exibir_andamento('Rede com poucos nos', logging.WARNING)
    else:
        nl.exibir_andamento("Não processado - Tamanho (kb): " + str(s[1]['size'] / 1024) + " - " + nomeArquivoPares)

#for s in saidaordenada.items():
#    print s[1]['size']
    #print s