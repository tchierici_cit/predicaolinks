import os
import re
import sys

import requests

reload(sys)
sys.setdefaultencoding('utf8')


# Metodo para salvar pdfs
def download_file(url, local_filename):
    cabecalho = {
        'user-agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36'}

    r = requests.get(url, cabecalho, stream=True, timeout=40)
    print url
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                # f.flush() commented by recommendation from J.F.Sebastian
    return local_filename


# Busca novas referencias nos arquivos DOI
def buscar_referencias(arquivo, doi):
    arquivo = open(arquivo)
    conteudo = arquivo.read()

    # procura links diretos para pdfs:
    busca = re.findall("[\"\']([^,\",\']*?pdfft[^,\",\']*?\.pdf)[\"\']", conteudo)
    for i in range(0, len(busca)):
        nomeArquivoPDF = "d:\\pdfd\\" + doi + ".pdf"
        print 'PDF encontrado'
        if (os.path.isfile(nomeArquivoPDF)):
            print("PDF ja existia")
        else:
            try:
                download_file(busca[0], nomeArquivoPDF)
                print("PDF salvo")
            except Exception as e:
                print("Problema ao baixar pdf")
                print e
    arquivo.close()
    return ""


processados = 0
listaArtigos = os.listdir("d:\\doi")
for artigo in listaArtigos:
    if artigo.endswith(".htm"):
        processados += 1
        buscar_referencias("d:\\doi\\" + artigo, artigo[:-4])
        print processados, " processados"
next

print 'fim'
