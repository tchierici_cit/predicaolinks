# -*- coding: utf8 -*-
from networkLib import *
import sys
import logging
reload(sys)
sys.setdefaultencoding('utf-8')

diretorioRaiz = "F:\\pesquisa\\MS\\exptamanho\\{0}\\"


# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="precisao" )

# Configuracoes utilizadas
codigos_fisica = [
    '0067A082',
    '071B0B18',
    '01FE4C7D',
    '09CF17C8',
    '020B5387',
    '08DD21F7',
    '05FA7120',
    '0AC86E3D']
codigos_saude = [
    '0C43E1FE',
    '036C9D1C',
    '0ADB48AF',
    '001BA264',
    '00E8E212']

nl.execucao_mag(diretorioRaiz.format("fisica"), reversed(codigos_fisica), 'Passado física: ', 2012, 2014)
nl.execucao_mag(diretorioRaiz.format("saude"), reversed(codigos_saude), 'Passado endocri: ', 2012, 2014)
nl.execucao_mag(diretorioRaiz.format("fisica"), reversed(codigos_fisica), 'Futuro física: ', 2015, 2015)
nl.execucao_mag(diretorioRaiz.format("saude"), reversed(codigos_saude), 'Futuro endocri: ', 2015, 2015)


#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)