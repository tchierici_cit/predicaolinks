# -*- coding: utf8 -*-
from networkLib import *
import sys
import logging
reload(sys)
sys.setdefaultencoding('utf-8')

diretorioRaiz = "F:\\pesquisa\\MS\\expdensidades\\{0}\\"


# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="densidades" )

# Configuracoes utilizadas
codigos_fisica = [
    '0BE3EBC9',
    '3DEC3564',
    '0C18E6EB',
    '044C55F7',
    '392B5DA7',
    '082B19A5']
codigos_computacao = [
    '0449514C',
    '092725CB',
    #'024E2B62',
    '039952EE',
    '03EFACE8']

nova_computacao = ['00050D34',
'0BD9D00C',
'0206F6F6',
'0366B3F8',
'057EDBCB',
'0B5D8EC1']

nova_comp2 =['095DDDE1',
'0AD011A3',
'09BB99E6',
'041F05A5',
'0A0407AE',
'026C1BE9',
'06DAD4AA',
'09DD8582',
'0923A056'
]



saude = ['09C9E1B5',
'016ABD6C',
'08CBFD8E',
'07403024',
'08EF545D',
'06E8D008',
'0ABB5D8C',
'068F622F',
'07648D5F',
'06A342D7',
'00D54B25']


varios = ['0A5E2C3B',
'02AED7A1',
'028EEF75',
'084C38C4',
'02C1583A',
'08B76997',
'0A228D0C',
'08938B0E',
'09CC34E7',
'0B703747',
'06E147CF',
'0763C7D1',
'032D420B',
'08ACED6D',
'06DF80C1',
'090520EF',
'0B075C5F'
]

#nl.execucao_mag(diretorioRaiz.format("fisica"), (codigos_fisica), 'Passado física: ', 2012, 2014)
#nl.execucao_mag(diretorioRaiz.format("computacao"), (codigos_computacao), 'Passado comp: ', 2012, 2014)
#nl.execucao_mag(diretorioRaiz.format("fisica"), (codigos_fisica), 'Futuro física: ', 2015, 2015)
#nl.execucao_mag(diretorioRaiz.format("computacao"), (codigos_computacao), 'Futuro comp: ', 2015, 2015)

#nl.execucao_mag(diretorioRaiz.format("nova_comp2"), (nova_comp2), 'Passado comp: ', 2012, 2014, old=True)
#nl.execucao_mag(diretorioRaiz.format("nova_comp2"), (nova_comp2), 'Futuro comp: ', 2015, 2015, old=True)

#nl.execucao_mag(diretorioRaiz.format("saude"), (saude), 'Passado saude: ', 2012, 2014, old=True)
#nl.execucao_mag(diretorioRaiz.format("saude"), (saude), 'Futuro saude: ', 2015, 2015, old=True)

nl.execucao_mag(diretorioRaiz.format("varios"), (varios), 'Passado varios: ', 2012, 2014, old=True)
nl.execucao_mag(diretorioRaiz.format("varios"), (varios), 'Futuro varios: ', 2015, 2015, old=True)

#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)