# -*- coding: iso8859_2 -*-
"""
Random graph from given degree sequence.
Draw degree rank plot and graph with matplotlib.
"""
import os.path
import fnmatch
import json
from unicodedata import normalize
from networkLib import *
reload(sys)
sys.setdefaultencoding('iso8859_2')
#sys.setdefaultencoding('utf-8')
from azure.storage.blob import BlockBlobService


# Configuracoes utilizadas

diretorioBase = "F:\\Hieraq-CNPQid\\"
diretorioNomesCompletos = "F:\\Pesquisa\\Hierarq-EvePerBD\\"

block_blob_service = BlockBlobService(account_name='fumeclattes',
                                      account_key='Ty1sOqMSH2srEq9dMhyF8ZisPazBiXUCp3slp3yU89NooY7U8u46Su9ircp+rEiiGghvmF/WbL+Cpg0eSBbgbg==')

def encontrar_nome(codigo):
    directory = ''
    for root, dirnames, filenames in os.walk(diretorioNomesCompletos):
        for dirname in fnmatch.filter(dirnames, codigo + '*'):
            directory = os.path.join(root, dirname)

    m = re.search(r"(?P<codigo>.\d{7}) - (?P<nome>[^\\]*?)$", directory)
    if m:
        directory = m.group("nome")
    try:
        retorno = normalize('NFKD', directory.decode('iso8859_2')).encode('ASCII', 'ignore')
    except UnicodeEncodeError:
        retorno = normalize('NFKD', directory).encode('ASCII', 'ignore')

    print retorno
    return retorno

def monta_json(retorno, dirbase):
    #print dirbase
    #print os.listdir(dirbase.encode('iso8859_2'))
    # passa por cada subdiretorio
    for dirarq in os.listdir(dirbase.encode('iso8859_2')):
        fulldir = os.path.join(dirbase, dirarq)
        if os.path.isdir(fulldir):
            dir = dirarq
            m = re.search(r"(?P<name>.\d{7})$", dir)
            if m:
                area = m.group(0).strip(' ')
                child = {}
                child["code"] = area
                child["area"] = encontrar_nome(area)
                child["children"] = []

                retorno["children"].append(child)

                # chama a recursividade
                retorno["children"][len(retorno["children"])-1] = monta_json(retorno["children"][len(retorno["children"])-1], fulldir)

    return retorno

def enviar_ids():
    #enviar = False
    global block_blob_service
    for root, directories, filenames in os.walk(diretorioBase.encode('iso8859_2')):
        for d in directories:

            m = re.search(r"(?P<name>.\d{7})$", d)
            if m:
                area = m.group(0).strip(' ')

                #if enviar:
                filename = os.path.join(root, "ids.txt")
                block_blob_service.create_blob_from_path(
                    'idsets',
                    ('%s' % area) + '/ids.txt',
                    '%s' % filename)
                print area

def renomeia_pastas():
    # Renomear pastas, deixando apenas o ID (sem o nome) de cada �rea
    print diretorioBase.encode('iso8859_2')
    print os.path.isdir(diretorioBase.encode('iso8859_2'))
    for root, directories, filenames in os.walk(diretorioBase.encode('iso8859_2')):

        for i in range(1, 5):
            for d in directories:
                # print d.encode('iso8859_2')

                dir = os.path.join(root.encode('iso8859_2'), d.encode('iso8859_2'))
                if (dir.rfind(" - ") > 0):
                    os.rename(dir, dir[0:dir.rfind(" - ")])
                # m = re.search(r"(?P<name>.\d{7})\s-[\w\s]+$", root.encode('utf8'))
                # if m:
                # print dir[0:dir.rfind(" - ")]
                print dir


retorno = {}
retorno["code"] = "00000000"
retorno["area"] = "Lattes inteiro"
retorno["children"] = []

retorno = monta_json(retorno, diretorioBase)
print json.dumps(retorno)

print 'fim'

