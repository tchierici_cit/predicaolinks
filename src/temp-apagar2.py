# -*- coding: utf8 -*-
import logging

from azure.storage.blob import BlockBlobService
import pandas as pd
import os
import networkx as nx
from networkLib import *

basedir = "f:\\pesquisa\\mag\\"
nl = NetworkLib(TipoArmazenamento.local, experimento="integrar" )
i = 0

for root, dirs, files in os.walk(basedir):
    for file in files:
        i = i + 1
        nl.exibir_andamento('Processando: '+ str(i))
        nomeArquivoPares = os.path.join(root, file)
        arquivoStats= os.path.join(root, 'stats-2012-2014.csv')
        statinfo = os.stat(nomeArquivoPares)
        if statinfo.st_size > 1 and statinfo.st_size< 1024 * 1024 * 40 and not (os.path.isfile(arquivoStats)): #40Mb
            nl.exibir_andamento("Nao processado - Tamanho (kb): " + str(statinfo.st_size / 1024) + " - " + nomeArquivoPares, logging.DEBUG)
            passado = pd.read_csv(nomeArquivoPares, names=['a', 'b'], dtype={'a': str, 'b': str}, sep=';',
                                  header=None)
            amplo = nx.from_pandas_dataframe(passado, 'a', 'b')
            G = nx.Graph(amplo)
            if G.number_of_edges() > 1:
                try:
                    nl.imprimir_stats(G, G.number_of_nodes(), arquivoStats)
                except Exception as e:
                    nl.exibir_andamento('Erro ao gerar stats '+ e.message + nomeArquivoPares, logging.ERROR)
            else:
                nl.exibir_andamento('Rede com poucos nos', logging.WARNING)
        else:
            nl.exibir_andamento("Não processado - Tamanho (kb): " + str(statinfo.st_size / 1024) + " - " + nomeArquivoPares)