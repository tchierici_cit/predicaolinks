# -*- coding: utf8 -*-
"""
Random graph from given degree sequence.
Draw degree rank plot and graph with matplotlib.
"""
import os.path
from networkLib import *
import pandas as pd
import networkx as nx
import sys
import logging
reload(sys)
import re
#sys.setdefaultencoding('iso8859_2')
sys.setdefaultencoding('utf-8')

# Configuracoes utilizadas
diretorioBase = "F:\\pesquisa\\mag\\"


def expand_list(original_list, total_len):
    for i in range(len(original_list), total_len):
        original_list.append("")
    return original_list



nl = NetworkLib(TipoArmazenamento.local)
nl.integrar_csvs(diretorioBase, "stats_completa.csv", 2012, 2014, "Periódicos MAG")