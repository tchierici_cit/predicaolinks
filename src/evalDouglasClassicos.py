# -*- coding: utf-8 -*-
# https://notebooks.azure.com/n/jzpUYscdU54/notebooks/1%20-%20Compare%20with%20spectral%20evolution.ipynb
from networkLib import *

nl = NetworkLib(TipoArmazenamento.azureblob, TipoToken.cnpqid, "DouglasClassicos", forcarGravacao=True,
                accountName='fumeclattes',
                accountKey='Ty1sOqMSH2srEq9dMhyF8ZisPazBiXUCp3slp3yU89NooY7U8u46Su9ircp+rEiiGghvmF/WbL+Cpg0eSBbgbg==')

# Configuracoes utilizadas
containerBase = "expteste" #"F:\\BD\\Sel\\Douglas\\"
nl.gerarOctave = False  # Indica se deve gerar o arquivo no formato Octave
nl.gerarPares = True  # Indica se deve gerar o arquito no formato de pares
nl.agruparAnos = True
anoInicialPassado = 2011
anoFinalPassado = 2012
anoInicialFuturo = 2013
anoFinalFuturo = 2013

# Gera o passado
nl.AnoInicial = anoInicialPassado
nl.AnoFinal = anoFinalPassado
# Vai gerar os pares de pesquisadores que se conectaram no intervalo inicial
nl.processar_lista(containerBase)

# Gera o futuro
nl.AnoInicial = anoInicialFuturo
nl.AnoFinal = anoFinalFuturo
# Vai gerar os pares de pesquisadores que se conectaram no intervalo final e gerar a lista dos pares que são novos*
nl.processar_lista(containerBase, gerarDiferencial=True, anoInicialPassado=anoInicialPassado, anoFinalPassado=anoFinalPassado)

nl.predizer_lista(containerBase, anoInicialPassado, anoFinalPassado, anoInicialFuturo, anoFinalFuturo)


print 'fim'



