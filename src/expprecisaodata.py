# -*- coding: utf8 -*-
from networkLib import *
import logging

diretorioRaiz = "F:\\pesquisa\\MS\\expprecisao-novo3\\"

# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="precisao" )

# Configuracoes utilizadas
codigos_pub = ['0BFBFFD3',
'088BD3E5',
'00DEE634',
'00B88E90',
'0AB7E63B',
'066497DA',
'08D9B1EF',
'0AD9B007'
               ]

codigos_pub = ['0004D416',
'0B0B69A3',
'0A614CB9',
'0358F6F9']

codigos_pub = ['0282BF76',
'09F4F008',
'06C0CC17',
'00EC0FF6',
'0A7CBA64',
'0BF7AB2E']


# Estração futuro 2015 janeiro
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 4: ', 2015, 2015, mes=1, old=True)

# Estração futuro 2015 Q1
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 3: ', 2015, 2015, trimestre=1, old=True)

# Extração futuro 2015 ano completo
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 2: ', 2015, 2015, old=True)

# Extração passados
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 1: ', 2012, 2014, old=True)



#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)