# -*- coding: utf8 -*-
from networkLib import *
import sys
import logging
reload(sys)
sys.setdefaultencoding('utf-8')

diretorioRaiz = "F:\\pesquisa\\MS\\expareas\\{0}\\"


# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="areas" )

# Configuracoes utilizadas
codigos_grandes_areas = []
codigos_areas = []
codigos_periodicos = [
    '03DB8409',
    '0057CCD3',
    '0296C1F9',
    '03DE2185',
    '03A92B6C',
    '053FCA5F',
    '06426316']

codigos_periodicos = [
'03184B72', '066DCC84', '05FFEEB0', #bio
'0C4B8B19', '06FA90E6', '09F53499', #agro
'0197B2A2', '009596B0', '069E5E40', #saude
'0303699A', '04FAFC8C', '06BA9163', #eng
'09A11514', '01FC784F', '0C2D73ED', #csa
'0080045D', '01734933', '01206F2B' #exata
]

codigos_periodicos = [
'083795E4', '06BCFD6A', '009B9054', '0ADEF1FF', #bio
'08FC4EAB', '0C5E26D8', '06E9FEF9', '0982D26E', #agro
'0BD8591C', '0B7D629B', '0A3D0894', '01070CCE', #saude
'07C03E40', '09354B80', '0B0CD089', '06D31EEF', #eng
'02397738', '086A4EF2', '0B04154B', '02280889', '0ABC84C6', #csa
'084E4DB6', '03C7498F', '096F7E2A', '00ECE999' #exata
]

nl.execucao_mag(diretorioRaiz.format("mag3"), (codigos_periodicos), 'Futuro periodicos: ', 2015, 2015)
nl.execucao_mag(diretorioRaiz.format("mag3"), (codigos_periodicos), 'Passado periodicos: ', 2012, 2014)

nl.exibir_andamento('Fim geral', logging.INFO)