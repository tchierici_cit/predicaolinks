# -*- coding: utf8 -*-
from networkLib import *
import logging
import re

diretorioRaiz = "F:\\pesquisa\\arxiv"

# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="arxiv",nivelLog=logging.INFO)

def juntar_xmls_diretorio(diretorio):
    for root, dirs, files in nl.listar(diretorio):
        nl.iniciar_gravar_arquivo(os.path.join(root, 'full.xml'))  # Container!?
        nl.continuar_gravar_arquivo('<?xml version="1.0" encoding="UTF-8"?><OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><ListRecords>')
        for f in files:
            file = os.path.join(root, f)
            conteudoXML = nl.obter_arquivo(file) # Container!?
            listaRecords = re.findall('<record>.*?</record>', conteudoXML, re.DOTALL)
            for tag in listaRecords:
                nl.continuar_gravar_arquivo(tag)
        nl.continuar_gravar_arquivo('</ListRecords></OAI-PMH>')
        nl.encerrar_gravar_arquivo()

def persistir_xmls_diretorio(diretorio):
    for root, dirs, files in nl.listar(diretorio):
        for f in files:
            file = os.path.join(root, f)
            conteudoXML = ET.fromstring(nl.obter_arquivo(file))[2] # Container!?
            records = conteudoXML.findall('{http://www.openarchives.org/OAI/2.0/}record')
            for record in records:
                header = record.find('{http://www.openarchives.org/OAI/2.0/}header')
                identifier = header.find('{http://www.openarchives.org/OAI/2.0/}identifier').text
                datestamp = header.find('{http://www.openarchives.org/OAI/2.0/}datestamp').text
                setSpec = header.find('{http://www.openarchives.org/OAI/2.0/}setSpec').text

                metadataArxiv = record.find('{http://www.openarchives.org/OAI/2.0/}metadata')
                arxiv = metadataArxiv.find('{http://arxiv.org/OAI/arXiv/}arXiv')

                id = arxiv.find('{http://arxiv.org/OAI/arXiv/}id').text
                created = arxiv.find('{http://arxiv.org/OAI/arXiv/}created').text
                if not arxiv.find('{http://arxiv.org/OAI/arXiv/}updated') is None:
                    updated = arxiv.find('{http://arxiv.org/OAI/arXiv/}updated').text
                title = arxiv.find('{http://arxiv.org/OAI/arXiv/}title').text
                comments = arxiv.find('{http://arxiv.org/OAI/arXiv/}comments').text
                abstract = arxiv.find('{http://arxiv.org/OAI/arXiv/}abstract').text

                # Insert pub

                authors = arxiv.find('{http://arxiv.org/OAI/arXiv/}authors')

                # Insert authors

for root, dirs, files in nl.listar(diretorioRaiz):
    for d in dirs:
        dir = os.path.join(root, d)
        nl.exibir_andamento(dir)
        persistir_xmls_diretorio(dir)

#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)

