# -*- coding: utf8 -*-
from networkLib import *
import logging

diretorioRaiz = "F:\\pesquisa\\MS\\expfaixatempo\\novo\\"

# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="faixatempo" )

# Configuracoes utilizadas
codigos_pub = [
    '0A139188',
    '047FBF7F',
    '0C3B94E4']

codigos_pub = ['09CF17C8', '09CFBD84']

# Extração futuro
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 6: ', 2015, 2015)
# Extração passado 1 ano
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 1: ', 2014, 2014)
# Extração passado 3 anos
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 2: ', 2012, 2014)
# Extração passado 5 anos
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 3: ', 2010, 2014)
# Extração passado 7 anos
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 4: ', 2008, 2014)
# Extração passado 10 anos
nl.execucao_mag(diretorioRaiz, codigos_pub, 'Fase 5: ', 2005, 2014)


#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)