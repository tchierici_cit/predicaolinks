# -*- coding: utf8 -*-
from networkLib import *
import sys
import logging
reload(sys)
sys.setdefaultencoding('utf-8')

diretorioRaiz = "F:\\pesquisa\\MS\\expdiametros\\{0}\\"


# Início disparo
nl = NetworkLib(TipoArmazenamento.local, experimento="diametros" )

# Configuracoes utilizadas
codigos_fisica = [
    '07B6A588',
    '08DD21F7',
    '083F692F',
    '076F738A',
    '0A39131E',
    '0925DE9D']
codigos_saude = [
    '02AED7A1',
    '08A8C3D6',
    '05C3851D',
    '0AC857F6',
    '011DC6EE']

novo_saude = ['0839A203', '06B533FB', '08C2B0B5', '056650FA', '081A3128', '0C2034D8']

saude4 = [
'0B799A6E',
'03B88CC5',
'08DA5DD0',
'0A91B56A',
'0AE6B4DC',
'049C8CED',
'075DCB5C',
'003AEBB6',
'05CA549D',
'06851DF6'
]

quimica = ['08CFCD04',
'09F837EB']

biologia = ['02A3BD26',
'0709CA63',
'07BFC841']
#nl.execucao_mag(diretorioRaiz.format("fisica"), (codigos_fisica), 'Passado física: ', 2012, 2014)
#nl.execucao_mag(diretorioRaiz.format("saude"), (codigos_saude), 'Passado saude: ', 2012, 2014)
#nl.execucao_mag(diretorioRaiz.format("fisica"), (codigos_fisica), 'Futuro física: ', 2015, 2015)
#nl.execucao_mag(diretorioRaiz.format("saude"), (codigos_saude), 'Futuro saude: ', 2015, 2015)


#nl.execucao_mag(diretorioRaiz.format("saude4"), (saude4), 'Futuro novo saude: ', 2015, 2015, old=True)
#nl.execucao_mag(diretorioRaiz.format("saude4"), (saude4), 'Passado novo saude: ', 2012, 2014, old=True)

nl.execucao_mag(diretorioRaiz.format("quimica"), (quimica), 'Futuro quimica: ', 2015, 2015, old=True)
nl.execucao_mag(diretorioRaiz.format("quimica"), (quimica), 'Passado quimica: ', 2012, 2014, old=True)

nl.execucao_mag(diretorioRaiz.format("biologia"), (biologia), 'Futuro biologia: ', 2015, 2015, old=True)
nl.execucao_mag(diretorioRaiz.format("biologia"), (biologia), 'Passado biologia: ', 2012, 2014, old=True)

#nl.integrar_csvs(diretorioRaiz, "stats_completa.csv", 2011, 2015, "Periódicos MAG")
nl.exibir_andamento('Fim geral', logging.INFO)