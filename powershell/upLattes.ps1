﻿$ContainerName = "expfaixatempo"

$idsLattes = '10300007'

$ctx = New-AzureStorageContext -StorageAccountName "fumeclattes" -StorageAccountKey "Ty1sOqMSH2srEq9dMhyF8ZisPazBiXUCp3slp3yU89NooY7U8u46Su9ircp+rEiiGghvmF/WbL+Cpg0eSBbgbg=="
$base = "lattes"

function Remove-Diacritics {
param ([String]$src = [String]::Empty)
  $normalized = $src.Normalize( [Text.NormalizationForm]::FormD )
  $sb = new-object Text.StringBuilder
  $normalized.ToCharArray() | % { 
    if( [Globalization.CharUnicodeInfo]::GetUnicodeCategory($_) -ne [Globalization.UnicodeCategory]::NonSpacingMark) {
      if ($_ -ne ' '){
        if ($_ -ne ','){
          [void]$sb.Append($_)
        }
      }
    }
  }
  $sb.ToString()
}

ForEach ($id in $idsLattes){
    Write-Output $id

    $files = Get-ChildItem -Directory -Recurse "F:\Pesquisa\lattesold\Hieraq-CNPQid" -Filter *$id*

    ForEach ($file in $files) { 
        $nomePasta = $file.Name | % { Remove-Diacritics $_ }
        $codigo = $nomePasta.split("-")[0]
        $area = $nomePasta.split("-")[1]
        $BlobName = $base + "-" + $area + "-" + $codigo + "/ids.txt"
        $arqIds = $file.FullName + "\ids.txt"

        Write-Output $BlobName
        Write-Output $arqIds
        Set-AzureStorageBlobContent -Force -File $arqIds -Container $ContainerName -Blob $BlobName -Context $ctx
    }
}

